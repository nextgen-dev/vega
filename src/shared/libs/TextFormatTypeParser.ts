import { TextFormatModel } from '../models/QuestionModel';

export const FormatModelToNumber = (format: TextFormatModel) => {
  switch (format) {
    case TextFormatModel.PLAIN:
      return 1;
    case TextFormatModel.HTML:
      return 2;
    case TextFormatModel.MARKDOWN:
      return 3;
    default:
      return 0;
  }
};

export const NumberToFormatModel = (format: number) => {
  switch (format) {
    case 1:
      return TextFormatModel.PLAIN;
    case 2:
      return TextFormatModel.HTML;
    case 3:
      return TextFormatModel.MARKDOWN;
    default:
      return TextFormatModel.UNKNOWN;
  }
};
