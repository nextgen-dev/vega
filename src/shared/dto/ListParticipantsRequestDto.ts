export default interface ListParticipantsRequestDto {
  size: number;
  page: number;
  examId: number;
}
