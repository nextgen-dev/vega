export interface GetAttemptRequestDto {
  id?: number;
  participantId?: number;
  examId?: number;
}
