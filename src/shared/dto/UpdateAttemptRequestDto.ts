import AttemptModel from '../models/AttemptModel';

export interface UpdateAttemptRequestDto {
  attempt: AttemptModel;
}
