import AttemptModel from '../models/AttemptModel';

export interface GetAttemptResponseDto {
  attempt: AttemptModel;
}
