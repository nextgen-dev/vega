export interface ListAttemptsRequestDto {
  filter?: { examId: number };
  size: number;
  page: number;
}
