import { createReducer } from '@reduxjs/toolkit';
import {
  createParticipant,
  deleteParticipant,
  listParticipants,
  setError,
  updateParticipant,
} from '../actions/ParticipantAction';
import { ParticipantModel } from '../models/ParticipantModel';
import { LoadingType } from '../models/LoadingType';

export interface ExamState {
  size: number;
  page: number;
  totalSize?: number;
  participants: ParticipantModel[];
  loading: LoadingType;
  error?: any;
}

const initialState: ExamState = {
  size: -1,
  page: 1,
  totalSize: undefined,
  participants: [],
  loading: LoadingType.Idle,
};

const ParticipantReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(listParticipants.pending, (state) => ({
      ...state,
      loading: LoadingType.GetList,
    }))
    .addCase(listParticipants.fulfilled, (state, action) => {
      return {
        ...state,
        size: action.payload.size,
        page: action.payload.page,
        participants: action.payload.participants,
        totalSize: action.payload.totalSize,
        loading: LoadingType.Idle,
      };
    })
    .addCase(listParticipants.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(createParticipant.pending, (state) => ({
      ...state,
      loading: LoadingType.Create,
    }))
    .addCase(createParticipant.fulfilled, (state, action) => {
      const existingParticipant = state.participants.find(
        (p) => p.id === action.payload.participant.id
      );
      if (!existingParticipant) {
        state.participants.push(action.payload.participant);
      }
      state.loading = LoadingType.Idle;
    })
    .addCase(createParticipant.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(updateParticipant.pending, (state) => ({
      ...state,
      loading: LoadingType.Update,
    }))
    .addCase(updateParticipant.fulfilled, (state, action) => {
      const updatedParticipantIndex = state.participants.findIndex(
        (participant) => participant.id === action.payload.participant.id
      );

      state.participants[updatedParticipantIndex] = action.payload.participant;
      state.loading = LoadingType.Idle;
    })
    .addCase(updateParticipant.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(deleteParticipant.pending, (state) => ({
      ...state,
      loading: LoadingType.Delete,
    }))
    .addCase(deleteParticipant.fulfilled, (state, action) => {
      const deletedParticipantIndex = state.participants.findIndex(
        (participant) => participant.id === action.payload.id
      );

      state.participants.splice(deletedParticipantIndex, 1);
      state.loading = LoadingType.Idle;
    })
    .addCase(deleteParticipant.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(setError, (state, action) => ({
      ...state,
      error: action.payload.err,
      loading: LoadingType.Idle,
    }))
);

export default ParticipantReducer;
