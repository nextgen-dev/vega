import { createReducer } from '@reduxjs/toolkit';
import { setError } from '../actions/AuthAction';
import {
  changeQuestion,
  clearQuestions,
  createQuestion,
  deleteQuestion,
  getQuestion,
  listQuestions,
  updateQuestion,
  updateQuestionType,
} from '../actions/QuestionAction';
import { QuestionModel } from '../models/QuestionModel';
import ChoiceModel from '../models/ChoiceModel';
import { LoadingType } from '../models/LoadingType';

export interface QuestionState {
  size: number;
  page: number;
  totalSize?: number;
  questions: QuestionModel[];
  activeQuestionIdx: number;
  activeQuestion?: QuestionModel;
  activeChoices?: ChoiceModel[];
  loading: LoadingType;
  error?: any;
}

const initialState: QuestionState = {
  size: -1,
  page: 1,
  totalSize: undefined,
  questions: [],
  activeQuestionIdx: 0,
  activeQuestion: undefined,
  activeChoices: undefined,
  loading: LoadingType.Idle,
  error: undefined,
};

const QuestionReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(listQuestions.pending, (state) => ({
      ...state,
      loading: LoadingType.GetList,
    }))
    .addCase(listQuestions.fulfilled, (state, action) => {
      return {
        ...state,
        size: action.payload.size,
        page: action.payload.page,
        questions: action.payload.questions,
        totalSize: action.payload.totalSize,
        loading: LoadingType.Idle,
      };
    })
    .addCase(listQuestions.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(getQuestion.pending, (state) => ({
      ...state,
      loading: LoadingType.GetEntry,
    }))
    .addCase(getQuestion.fulfilled, (state, action) => {
      return {
        ...state,
        activeQuestion: action.payload.question,
        loading: LoadingType.Idle,
      };
    })
    .addCase(getQuestion.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(createQuestion.pending, (state) => ({
      ...state,
      loading: LoadingType.Create,
    }))
    .addCase(createQuestion.fulfilled, (state, action) => {
      state.questions.push(action.payload.question);
      console.log(state.questions);
      state.activeQuestion = action.payload.question;
      state.activeQuestionIdx = state.questions.length - 1;
      state.loading = LoadingType.Idle;
    })
    .addCase(createQuestion.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(updateQuestion.pending, (state) => ({
      ...state,
      loading: LoadingType.Update,
    }))
    .addCase(updateQuestion.fulfilled, (state, action) => {
      return {
        ...state,
        activeQuestion: action.payload.question,
        loading: LoadingType.Idle,
      };
    })
    .addCase(updateQuestion.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(updateQuestionType.pending, (state) => ({
      ...state,
      loading: LoadingType.Update,
    }))
    .addCase(updateQuestionType.fulfilled, (state, action) => {
      const updatedQuestionIdx = state.questions.findIndex(
        (q) => q.id === action.payload.oldId
      );

      state.questions[updatedQuestionIdx] = action.payload.question;
      state.activeQuestion = action.payload.question;
      state.loading = LoadingType.Idle;
    })
    .addCase(updateQuestionType.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(deleteQuestion.pending, (state) => ({
      ...state,
      loading: LoadingType.Delete,
    }))
    .addCase(deleteQuestion.fulfilled, (state, action) => {
      const deletedQuestionIndex = state.questions.findIndex(
        (question) => question.id === action.payload.id
      );
      state.questions.splice(deletedQuestionIndex, 1);

      if (state.activeQuestionIdx != state.questions.length - 1) {
        state.activeQuestionIdx = deletedQuestionIndex - 1;
      }

      state.loading = LoadingType.Idle;
    })
    .addCase(deleteQuestion.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(changeQuestion, (state, action) => {
      state.activeQuestionIdx = action.payload.index;
      state.loading = LoadingType.Idle;
    })
    .addCase(clearQuestions, (state) => ({
      ...state,
      questions: [],
      loading: LoadingType.Idle,
    }))
    .addCase(setError, (state, action) => ({
      ...state,
      error: action.payload.err,
      loading: LoadingType.Idle,
    }))
);

export default QuestionReducer;
