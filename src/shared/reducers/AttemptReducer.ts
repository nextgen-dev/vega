import { createReducer } from '@reduxjs/toolkit';
import AttemptModel from '../models/AttemptModel';
import { LoadingType } from '../models/LoadingType';
import ErrorResponseDto from '../dto/ErrorResponseDto';
import attemptAction from '../actions/AttemptAction';

export interface AttemptState {
  attempts: AttemptModel[];
  loading: LoadingType;
  error?: ErrorResponseDto;
}

const initialState: AttemptState = {
  attempts: [],
  loading: LoadingType.Idle,
  error: undefined,
};

const AttemptReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(attemptAction.listAttempts.pending, (state) => {
      return {
        ...state,
        loading: LoadingType.GetList,
      };
    })
    .addCase(attemptAction.listAttempts.fulfilled, (state, action) => {
      return {
        ...state,
        attempts: action.payload.attempts,
        loading: LoadingType.Idle,
      };
    })
    .addCase(attemptAction.listAttempts.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.Idle,
      };
    })
    .addCase(attemptAction.getAttempt.pending, (state) => {
      return {
        ...state,
        loading: LoadingType.GetEntry,
      };
    })
    .addCase(attemptAction.getAttempt.fulfilled, (state, action) => {
      if (action.payload.attempt) {
        state.attempts.push(action.payload.attempt);
      }
      state.loading = LoadingType.Idle;
    })
    .addCase(attemptAction.getAttempt.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.Idle,
      };
    })
    .addCase(attemptAction.updateAttempt.pending, (state) => {
      return {
        ...state,
        loading: LoadingType.Update,
      };
    })
    .addCase(attemptAction.updateAttempt.fulfilled, (state, action) => {
      const updatedAttemptIndex = state.attempts.findIndex(
        (attempt) => attempt.id === action.payload.attempt.id
      );

      state.attempts[updatedAttemptIndex] = action.payload.attempt;
      state.loading = LoadingType.Idle;
    })
    .addCase(attemptAction.updateAttempt.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.Idle,
      };
    })
    .addCase(attemptAction.setError, (state, action) => {
      return {
        ...state,
        error: action.payload,
        loading: LoadingType.Idle,
      };
    })
    .addCase(attemptAction.resetState, () => {
      return {
        ...initialState,
      };
    })
);

export default AttemptReducer;
