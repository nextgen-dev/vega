import { createReducer } from '@reduxjs/toolkit';
import {
  changeTab,
  createTab,
  removeTab,
  saveNewTab,
  updateTab,
} from '../actions/TabAction';

export interface TabProps {
  id: number;
  newExamTab: boolean;
  newExam?: {
    title: string;
    password: string;
    startAt: string;
    timeLimit: number;
  };
  title: string;
}

export interface TabState {
  tabs: TabProps[];
  activeTab?: TabProps;
}

const initialState: TabState = {
  tabs: [],
  activeTab: undefined,
};

const TabReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(createTab, (state, action) => {
      const { newExam, exam } = action.payload;
      let newTab: TabProps;
      if (newExam) {
        const newId =
          state.tabs.length < 1 ? 1 : state.tabs[state.tabs.length - 1].id + 1;
        newTab = { id: newId, title: 'New Exam', newExamTab: true };
        state.tabs.push(newTab);
        state.activeTab = newTab;
      }
      if (exam) {
        const sameIdTab = state.tabs.find((tab) => tab.id === exam.id);
        if (sameIdTab) {
          sameIdTab.id += exam.id + 1000;
        }
        newTab = { id: exam.id, title: exam.title, newExamTab: false };
        state.tabs.push(newTab);
        state.activeTab = newTab;
      }
    })
    .addCase(changeTab, (state, action) => {
      const selectedTab = state.tabs.find(
        (tab) => tab.id === action.payload.id
      );
      return {
        ...state,
        activeTab: selectedTab,
      };
    })
    .addCase(removeTab, (state, action) => {
      const deletedTab = state.tabs.find(
        (tab) => tab.id === action.payload.id
      ) as TabProps;
      const deletedTabIndex = state.tabs.indexOf(deletedTab);
      state.tabs.splice(deletedTabIndex, 1);

      if (deletedTab.id === state.activeTab?.id) {
        if (state.tabs.length > 1) {
          state.activeTab = state.tabs.find(
            (_tab, index) => index === deletedTabIndex - 1
          );
        } else {
          state.activeTab = undefined;
        }
      }
    })
    .addCase(saveNewTab, (state, action) => {
      const activeTabIndex = state.tabs.findIndex(
        (tab) => tab.id === state.activeTab?.id
      );
      state.tabs[activeTabIndex].newExam = action.payload.newExam;
      state.tabs[activeTabIndex].title = action.payload.newExam.title;
    })
    .addCase(updateTab, (state, action) => {
      const updatedTabIndex = state.tabs.findIndex(
        (tab) => tab.id === state.activeTab?.id
      );
      state.tabs[updatedTabIndex].id = action.payload.id;
      state.tabs[updatedTabIndex].title = action.payload.title;
      state.tabs[updatedTabIndex].newExamTab = false;
      state.tabs[updatedTabIndex].newExam = undefined;
    });
});

export default TabReducer;
