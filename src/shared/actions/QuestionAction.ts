import { createAction } from '@reduxjs/toolkit';
import { QuestionModel } from '../models/QuestionModel';

export const listQuestions = {
  pending: createAction('[Question] List all questions/pending'),
  fulfilled: createAction<{
    size: number;
    page: number;
    questions: QuestionModel[];
    totalSize: number;
  }>('[Question] List all questions/fulfilled'),
  rejected: createAction<{ err: any }>(
    '[Question] List all questions/rejected'
  ),
};

export const getQuestion = {
  pending: createAction('[Question] Get one question/pending'),
  fulfilled: createAction<{ question: QuestionModel }>(
    '[Question] Get one question/fulfilled'
  ),
  rejected: createAction<{ err: any }>('[Question] Get one question/rejected'),
};

export const createQuestion = {
  pending: createAction('[Question] Create new question/pending'),
  fulfilled: createAction<{ question: QuestionModel }>(
    '[Question] Create new question/fulfilled'
  ),
  rejected: createAction<{ err: any }>(
    '[Question] Create new question/rejected'
  ),
};

export const updateQuestion = {
  pending: createAction('[Question] Update one question/pending'),
  fulfilled: createAction<{ question: QuestionModel }>(
    '[Question] Update one question/fulfilled'
  ),
  rejected: createAction<{ err: any }>(
    '[Question] Update one question/rejected'
  ),
};

export const updateQuestionType = {
  pending: createAction('[Question] Update one question type/pending'),
  fulfilled: createAction<{ question: QuestionModel; oldId: number }>(
    '[Question] Update one question type/fulfilled'
  ),
  rejected: createAction<{ err: any }>(
    '[Question] Update one question type/rejected'
  ),
};

export const deleteQuestion = {
  pending: createAction('[Question] Delete one question/pending'),
  fulfilled: createAction<{ id: number }>(
    '[Question] Delete one question/fulfilled'
  ),
  rejected: createAction<{ err: any }>(
    '[Question] Delete one question/rejected'
  ),
};

export const clearQuestions = createAction('[Question] Clear question list');

export const changeQuestion = createAction<{ index: number }>(
  '[Question] change active question'
);

export const setQuestionError = createAction<{ err: any }>(
  '[Question] set question error'
);
