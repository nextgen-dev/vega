import { createAction } from '@reduxjs/toolkit';
import { ParticipantModel } from '../models/ParticipantModel';

export const listParticipants = {
  pending: createAction('[Participant] List all participants/pending'),
  fulfilled: createAction<{
    size: number;
    page: number;
    participants: ParticipantModel[];
    totalSize: number;
  }>('[Participant] List all participants/fulfilled'),
  rejected: createAction<{ err: any }>(
    '[Participant] List all participants/rejected'
  ),
};

export const createParticipant = {
  pending: createAction('[Participant] Create new participant/pending'),
  fulfilled: createAction<{ participant: ParticipantModel }>(
    '[Exam] Create new participant/fulfilled'
  ),
  rejected: createAction<{ err: any }>(
    '[Participant] Create new participant/rejected'
  ),
};

export const updateParticipant = {
  pending: createAction('[Participant] Update one participant/pending'),
  fulfilled: createAction<{ participant: ParticipantModel }>(
    '[Exam] Update one participant/fulfilled'
  ),
  rejected: createAction<{ err: any }>(
    '[Participant] Update one participant/rejected'
  ),
};

export const deleteParticipant = {
  pending: createAction('[Participant] Delete one participant/pending'),
  fulfilled: createAction<{ id: number }>(
    '[Participant] Delete one participant/fulfilled'
  ),
  rejected: createAction<{ err: any }>(
    '[Participant] Delete one participant/rejected'
  ),
};

export const setError = createAction<{ err: any }>(
  '[Participant] set participant error'
);
