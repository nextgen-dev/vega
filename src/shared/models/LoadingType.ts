export enum LoadingType {
  Idle,
  GetList,
  GetEntry,
  Update,
  Delete,
  Create,
}

export const LoadingTypeToString = (
  type: LoadingType,
  context: string
): string => {
  switch (type) {
    case LoadingType.Create:
      return `Creating ${context}`;
    case LoadingType.Delete:
      return `Deleting ${context}`;
    case LoadingType.GetList:
      return `Loading ${context} list`;
    case LoadingType.GetEntry:
      return `Loading ${context}`;
    case LoadingType.Update:
      return `Updating ${context}`;
    default:
      return `Loading`;
  }
};
