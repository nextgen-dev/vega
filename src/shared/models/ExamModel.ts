export enum ExamStatusModel {
  UNRECOGNIZED,
  WAITING,
  STARTED,
  DONE,
}

export interface ExamModel {
  id: number;
  password: string;
  title: string;
  timeLimit: number;
  startAt: string | undefined;
  status: ExamStatusModel;
}
