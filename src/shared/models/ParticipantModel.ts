export interface ParticipantModel {
  id: number;
  examId: number;
  code: string;
  name: string;
}
