import React from 'react';
import { MemoryRouter as Router, Route, Switch } from 'react-router-dom';
import './App.global.css';
import Login from './views/Login';
import Dashboard from './views/Dashboard';
import ExamWrapper from './components/ExamWrapper';
import {
  authListener,
  examListener,
  participantListener,
  questionListener,
} from './ipcRenderer/listeners';
import choiceListener from './ipcRenderer/listeners/choiceListener';
import attemptListener from './ipcRenderer/listeners/attemptListener';

declare global {
  interface Window {
    auth?: any;
    exam?: any;
    participant?: any;
    question?: any;
    choice?: any;
    attempt?: any;
  }
}

authListener();
examListener();
participantListener();
questionListener();
choiceListener();
attemptListener();

export default function App() {
  return (
    <Router>
      <Switch>
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/exam/:id" component={ExamWrapper} />
        <Route path="/" component={Login} />
      </Switch>
    </Router>
  );
}
