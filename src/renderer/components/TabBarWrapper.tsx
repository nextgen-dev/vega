import React from 'react';
import { Layout } from 'antd';
import TabBar from './TabBar';

interface Props {
  children: React.ReactNode;
}

export default function TabBarWrapper(props: Props) {
  const { Content, Header } = Layout;
  const { children } = props;

  return (
    <Layout style={{ maxHeight: '100vh' }}>
      <Header
        style={{
          padding: 0,
          height: '4vh',
          width: '100vw',
          position: 'fixed',
          zIndex: 10,
        }}
      >
        <TabBar />
      </Header>
      <Content style={{ marginTop: '4vh', minHeight: '96vh' }}>
        {children}
      </Content>
    </Layout>
  );
}
