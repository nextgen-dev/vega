import React, { useEffect, useState } from 'react';
import { Col, Form, Radio, Space, Typography } from 'antd';
import {
  QuestionModel,
  QuestionTypeModel,
  TextFormatModel,
} from '../../shared/models/QuestionModel';
import ChoiceModel from '../../shared/models/ChoiceModel';
import { TypeModelToString } from '../../shared/libs/QuestionTypeParser';
import ReactQuill from 'react-quill';
import {
  requestUpdateQuestion,
  requestUpdateQuestionType,
} from '../ipcRenderer/senders/questionSender';
import QuestionEditControl from './QuestionEditControl';
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import ChoiceForm from './ChoiceForm';
import { formats, modules } from '../../shared/libs/reactQuillConfig';

export default function QuestionForm(props: {
  question: QuestionModel;
  choices: ChoiceModel[];
}): JSX.Element {
  const { Title } = Typography;

  const { question, choices } = props;

  const activeQuestionIdx = useSelector(
    (state: RootState) => state.QuestionReducer.activeQuestionIdx
  );

  const [formQuestion, setFormQuestion] = useState({
    content: question.content,
    format: TextFormatModel.HTML,
    type: question.type,
  });

  const onValuesChange = (_changedValues: any, allValues: any) => {
    setFormQuestion((prevState) => {
      return {
        ...prevState,
        content: allValues.content,
        type: allValues.type,
      };
    });
  };

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (question) {
        if (
          formQuestion.content !== '' &&
          formQuestion.content != question?.content
        ) {
          if (formQuestion.type == question?.type) {
            return requestUpdateQuestion({
              id: question.id,
              examId: question.examId,
              content: formQuestion.content,
              format: formQuestion.format,
              type: formQuestion.type,
            });
          } else {
            return requestUpdateQuestionType({
              id: question.id,
              examId: question.examId,
              content: formQuestion.content,
              format: formQuestion.format,
              type: formQuestion.type,
            });
          }
        }
        if (formQuestion.type != question?.type) {
          requestUpdateQuestionType({
            id: question.id,
            examId: question.examId,
            content: formQuestion.content,
            format: formQuestion.format,
            type: formQuestion.type,
          });
        }
      }
    }, 3000);
    return () => clearTimeout(timeout);
  }, [formQuestion, question]);

  const saveQuestion = () => {
    requestUpdateQuestion({
      id: question.id,
      examId: question.examId,
      content: formQuestion.content,
      format: formQuestion.format,
      type: formQuestion.type,
    });
  };

  const [questionForm] = Form.useForm();
  useEffect(() => {
    questionForm.setFieldsValue({
      ...questionForm.getFieldsValue(),
      content: question.content,
      type: question.type,
    });
    setFormQuestion((prevState) => {
      return {
        ...prevState,
        content: question.content,
        type: question.type,
      };
    });
  }, [question, choices, questionForm]);

  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <QuestionEditControl
        num={activeQuestionIdx + 1}
        saveQuestion={saveQuestion}
      />
      <Form
        onValuesChange={onValuesChange}
        initialValues={formQuestion}
        form={questionForm}
      >
        <Col>
          <Title level={4} style={{ fontWeight: 'bold' }}>
            Question
          </Title>
          <Form.Item name={'content'} style={{ width: '100%' }}>
            <ReactQuill
              style={{ width: '100%' }}
              modules={modules}
              formats={formats}
            />
          </Form.Item>
          <Title level={5} style={{ fontWeight: 'bold' }}>
            Question Type
          </Title>
          <Form.Item name={'type'} style={{ width: '100%' }}>
            <Radio.Group>
              <Col flex="100%">
                <Radio value={QuestionTypeModel.MC}>
                  {TypeModelToString(QuestionTypeModel.MC)}
                </Radio>
                <Radio value={QuestionTypeModel.ES}>
                  {TypeModelToString(QuestionTypeModel.ES)}
                </Radio>
              </Col>
            </Radio.Group>
          </Form.Item>
        </Col>
      </Form>
      {choices?.length !== 0 ? (
        <ChoiceForm questionId={question.id} choices={choices} />
      ) : (
        ''
      )}
    </Space>
  );
}
