import React, { useState } from 'react';
import { Button, Card, Col, Row, Spin, Typography } from 'antd';
import { EditFilled, LoadingOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { AppDispatch, RootState } from '../store';
import { SecondsToDuration } from '../../shared/libs/TimeParser';
import ExamForm from './ExamForm';
import { StatusModelToString } from '../../shared/libs/StatusParser';
import { updateTab } from '../../shared/actions/TabAction';
import { requestUpdateExam } from '../ipcRenderer/senders/examSender';
import { LoadingType } from '../../shared/models/LoadingType';

export default function ExamInfoSection(): JSX.Element {
  const { Title } = Typography;
  const exam = useSelector((state: RootState) => state.ExamReducer.activeExam);
  const examLoading = useSelector(
    (state: RootState) => state.ExamReducer.loading
  );

  const [editing, setEditing] = useState(false);
  const toggleEdit = () => setEditing((prevState) => !prevState);

  const initialState = () => {
    return {
      ...exam,
      startAt: moment(exam?.startAt),
      timeLimit: {
        hours: Math.floor((exam?.timeLimit || 0) / 3600),
        minutes: ((exam?.timeLimit || 0) / 60) % 60,
      },
    };
  };

  const dispatch = useDispatch<AppDispatch>();
  const handleSubmit = (values: any) => {
    requestUpdateExam({
      ...values,
      id: exam?.id,
      status: values.status,
    });
    if (exam && exam?.title !== values.title) {
      dispatch(updateTab({ id: exam?.id, title: values.title }));
    }
    toggleEdit();
  };

  return (
    <Spin
      indicator={<LoadingOutlined />}
      size="large"
      tip="Loading Exam Details..."
      spinning={examLoading != LoadingType.Idle}
    >
      <Card>
        {editing ? (
          <ExamForm
            newExam={false}
            initialState={initialState()}
            onChange={() => {}}
            onSubmit={handleSubmit}
            onCancel={toggleEdit}
          />
        ) : (
          <div>
            <Row>
              <Col>
                <Title style={{ color: '#175873' }}>{exam?.title}</Title>
              </Col>
            </Row>
            <Row>
              <Col span={3}>
                <Title level={5}>Exam Password</Title>
              </Col>
              <Col>
                <Title level={5}>: {exam?.password}</Title>
              </Col>
            </Row>
            <Row>
              <Col span={3}>
                <Title level={5}>Start At</Title>
              </Col>
              <Col>
                <Title level={5}>
                  : {moment(exam?.startAt).format('DD MMMM YYYY HH:mm')}
                </Title>
              </Col>
            </Row>
            <Row>
              <Col span={3}>
                <Title level={5}>Time Limit</Title>
              </Col>
              <Col>
                <Title level={5}>: {SecondsToDuration(exam?.timeLimit)}</Title>
              </Col>
            </Row>
            <Row>
              <Col span={3}>
                <Title level={5}>Status</Title>
              </Col>
              <Col>
                <Title level={5}>: {StatusModelToString(exam?.status)}</Title>
              </Col>
            </Row>
            <Button
              type="primary"
              style={{ marginTop: '20px' }}
              onClick={toggleEdit}
            >
              <EditFilled />
              EDIT
            </Button>
          </div>
        )}
      </Card>
    </Spin>
  );
}
