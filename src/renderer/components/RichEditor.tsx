import React, { useEffect } from 'react';
import { Input } from 'antd';

// const modules = {
//   toolbar: [
//     [{ header: '1' }, { header: '2' }, { font: [] }],
//     [{ size: [] }],
//     ['bold', 'italic', 'underline', 'strike', 'blockquote'],
//     [
//       { list: 'ordered' },
//       { list: 'bullet' },
//       { indent: '-1' },
//       { indent: '+1' },
//     ],
//     ['link', 'image', 'video'],
//     ['clean'],
//   ],
//   clipboard: {
//     // toggle to add extra line breaks when pasting HTML:
//     matchVisual: false,
//   },
// };
//
// const formats = [
//   'header',
//   'font',
//   'size',
//   'bold',
//   'italic',
//   'underline',
//   'strike',
//   'blockquote',
//   'list',
//   'bullet',
//   'indent',
//   'link',
//   'image',
//   'video',
// ];

export default function RichEditor(props: {
  value?: string;
  onChange?: (value: string) => void;
}): JSX.Element {
  const { value, onChange } = props;
  const { TextArea } = Input;

  useEffect(() => {
    if (value !== '') {
      console.log(value);
    }
  }, [value]);

  return (
    <div>
      <TextArea
        value={value}
        onChange={(event) => {
          if (onChange) {
            onChange(event.target.value);
          }
        }}
      />
      {/*<ReactQuill*/}
      {/*  style={{ width: '100%' }}*/}
      {/*  modules={modules}*/}
      {/*  formats={formats}*/}
      {/*  onChange={(content) => {*/}
      {/*    if (onChange) onChange(content);*/}
      {/*  }}*/}
      {/*  defaultValue={value}*/}
      {/*/>*/}
    </div>
  );
}
