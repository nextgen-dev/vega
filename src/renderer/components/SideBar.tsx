import React from 'react';
import { Layout, Menu } from 'antd';
import { NavLink, useRouteMatch } from 'react-router-dom';
import {
  FileTextOutlined,
  UnorderedListOutlined,
  DownloadOutlined,
} from '@ant-design/icons';

export default function SideBar(): JSX.Element {
  const { Sider } = Layout;
  const { url } = useRouteMatch();

  return (
    <Sider
      style={{
        height: '100vh',
        width: '13rem',
        position: 'fixed',
      }}
    >
      <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
        <Menu.Item key="1" icon={<FileTextOutlined />}>
          <NavLink to={`${url}/detail`}>Test Detail</NavLink>
        </Menu.Item>
        <Menu.Item key="2" icon={<UnorderedListOutlined />}>
          <NavLink to={`${url}/manage-question`}>Manage Questions</NavLink>
        </Menu.Item>
        <Menu.Item key="" icon={<DownloadOutlined />}>
          <NavLink to={`${url}/export-import`}>Export & Import</NavLink>
        </Menu.Item>
      </Menu>
    </Sider>
  );
}
