import { Space, Typography } from 'antd';
import React from 'react';
import { QuestionModel } from '../../shared/models/QuestionModel';
import ReactQuill from 'react-quill';

export default function QuestionItem(props: {
  index: number;
  question: QuestionModel;
}): JSX.Element {
  const { Paragraph } = Typography;

  const { question, index } = props;

  return (
    <Space direction="horizontal" align="start">
      <Paragraph>{index}</Paragraph>
      <ReactQuill value={question.content} readOnly theme="bubble" />
    </Space>
  );
}
