import React, { CSSProperties } from 'react';
import { Button, List, Space, Typography } from 'antd';
import { QuestionModel } from '../../shared/models/QuestionModel';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../store';
import { changeQuestion } from '../../shared/actions/QuestionAction';
import { LoadingType } from '../../shared/models/LoadingType';

const style = (active: boolean): CSSProperties => {
  const customButton: CSSProperties = {
    width: '3em',
    height: '3em',
    textAlign: 'center',
    padding: 0,
    margin: 0,
  };
  const colorPrimary: CSSProperties = {
    background: '#0642CD',
    borderColor: '#0642CD',
  };

  if (active) {
    return {
      ...customButton,
      ...colorPrimary,
    };
  }

  return {
    ...customButton,
  };
};

export default function QuestionList(props: {
  questions: QuestionModel[];
  activeIndex: number;
}): JSX.Element {
  const { Title } = Typography;
  const { questions, activeIndex } = props;
  const modifiedQuestion = questions.map((question, index) => {
    return {
      ...question,
      index,
    };
  });

  const questionLoading = useSelector(
    (state: RootState) => state.QuestionReducer.loading
  );
  const choiceLoading = useSelector(
    (state: RootState) => state.ChoiceReducer.loading
  );

  const dispatch = useDispatch<AppDispatch>();
  const changeActiveQuestion = (index: number) => {
    dispatch(changeQuestion({ index }));
  };

  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Title level={3} style={{ textAlign: 'center' }}>
        Question List
      </Title>
      <List
        grid={{ column: 5 }}
        dataSource={modifiedQuestion}
        renderItem={(question) => (
          <List.Item>
            <Button
              style={style(question.index === activeIndex)}
              type={question.index === activeIndex ? 'primary' : 'default'}
              onClick={() => changeActiveQuestion(question.index)}
              disabled={
                questionLoading != LoadingType.Idle ||
                choiceLoading != LoadingType.Idle
              }
            >
              {question.index + 1}
            </Button>
          </List.Item>
        )}
      />
    </Space>
  );
}
