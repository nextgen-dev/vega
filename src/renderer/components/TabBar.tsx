import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Tabs } from 'antd';
import { useHistory } from 'react-router-dom';
import { FileFilled, HomeFilled } from '@ant-design/icons';
import { AppDispatch, RootState } from '../store';
import {
  changeTab,
  createTab,
  removeTab,
} from '../../shared/actions/TabAction';

const TabBar = (): JSX.Element => {
  const { TabPane } = Tabs;
  const tabList = useSelector((state: RootState) => state.TabReducer.tabs);
  const activeTab = useSelector(
    (state: RootState) => state.TabReducer.activeTab
  );

  const dispatch = useDispatch<AppDispatch>();
  const history = useHistory();

  const onChangeTab = (key: string) => {
    dispatch(changeTab({ id: parseInt(key, 10) }));
    if (key === '0') {
      history.push(`/dashboard`);
    } else {
      history.push(`/exam/${key}/detail`);
    }
  };

  const onEditTab = (targetKey: any, action: string) => {
    if (action === 'add') {
      dispatch(createTab({ newExam: true }));
    } else {
      dispatch(removeTab({ id: parseInt(targetKey as string, 10) }));
    }
  };

  useEffect(() => {
    if (activeTab) {
      history.push(`/exam/${activeTab.id}/detail`);
    } else {
      history.push(`/dashboard`);
    }
  }, [activeTab, history]);

  return (
    <Tabs
      type="editable-card"
      style={{ margin: 0, height: '100%' }}
      tabBarStyle={{ margin: 0, height: '100%' }}
      onChange={onChangeTab}
      onEdit={onEditTab}
      activeKey={activeTab?.id.toString() || '0'}
    >
      <TabPane
        tab={
          <div style={{ width: '8rem', maxWidth: '8em' }}>
            <HomeFilled />
            Home
          </div>
        }
        key="0"
        closable={false}
      />
      {tabList.map((tab) => (
        <TabPane
          tab={
            <div style={{ width: '8rem', maxWidth: '8em', overflow: 'hidden' }}>
              <FileFilled />
              {tab.title}
            </div>
          }
          key={tab.id}
        />
      ))}
    </Tabs>
  );
};

export default TabBar;
