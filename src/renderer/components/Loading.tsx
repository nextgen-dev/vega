import React from 'react';
import { Row, Space, Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

export default function Loading(props: { context: string }): JSX.Element {
  const { context } = props;

  return (
    <Row
      align="middle"
      justify="center"
      style={{ height: '100%', width: '100%' }}
    >
      <Space align="center" direction="vertical">
        <Spin
          indicator={<LoadingOutlined />}
          size="large"
          tip={`${context}...`}
        />
      </Space>
    </Row>
  );
}
