import React, { useState } from 'react';
import AttemptEdit from './AttemptEdit';
import AttemptInfo from './AttemptInfo';
import AttemptModel from '../../shared/models/AttemptModel';
import attemptSender from '../ipcRenderer/senders/attemptSender';

export default function AttemptRow(props: {
  attempt: AttemptModel;
  toggleNewAttempt?: () => void;
}): JSX.Element {
  const { attempt } = props;

  const [editing, setEditing] = useState(false);
  const toggleEdit = () => {
    setEditing((prevState) => !prevState);
  };

  const handleSubmit = (values: any) => {
    if (values.finished !== attempt?.finished) {
      attemptSender.updateAttempt({
        attempt: {
          ...attempt,
          finished: values.finished,
        },
      });
    }
    toggleEdit();
  };

  return (
    <div>
      {editing || !attempt ? (
        <AttemptEdit
          attempt={attempt}
          toggleEdit={toggleEdit}
          handleSubmit={handleSubmit}
        />
      ) : (
        <AttemptInfo attempt={attempt} toggleEdit={toggleEdit} />
      )}
    </div>
  );
}
