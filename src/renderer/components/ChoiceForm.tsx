import React, { useEffect, useState } from 'react';
import { Col, Form, Radio, Typography } from 'antd';
import ReactQuill from 'react-quill';
import ChoiceModel from '../../shared/models/ChoiceModel';
import { formats, modules } from '../../shared/libs/reactQuillConfig';
import { requestUpdateChoice } from '../ipcRenderer/senders/choiceSender';
import { FormatModelToNumber } from '../../shared/libs/TextFormatTypeParser';
import { TextFormatModel } from '../../shared/models/QuestionModel';

export default function ChoiceForm(props: {
  questionId: number;
  choices: ChoiceModel[];
}): JSX.Element {
  const { questionId, choices } = props;

  const { Title } = Typography;

  const [formChoices, setFormChoices] = useState({
    choices: choices.map((choice) => ({
      id: choice.id,
      content: choice.content,
    })),
    correctChoice: choices.find((choice) => choice.isCorrect)?.id || -1,
  });

  const [choicesForm] = Form.useForm();
  useEffect(() => {
    choicesForm.setFieldsValue({
      choices: choices.map((choice) => ({
        id: choice.id,
        content: choice.content,
      })),
      correctChoice: choices.find((choice) => choice.isCorrect)?.id || -1,
    });
    setFormChoices({
      choices: choices.map((choice) => ({
        id: choice.id,
        content: choice.content,
      })),
      correctChoice: choices.find((choice) => choice.isCorrect)?.id || -1,
    });
  }, [choices, choicesForm]);

  const onValuesChange = (_changedValues: any, allValues: any) => {
    if (allValues.choices) {
      setFormChoices((prevState) => {
        return {
          ...prevState,
          choices: allValues.choices.map(
            (choice: { id: number; content: string }) => ({
              id: choice.id,
              content: choice.content,
            })
          ),
          correctChoice: allValues.correctChoice,
        };
      });
    }
  };

  useEffect(() => {
    const timeout = setTimeout(() => {
      formChoices.choices.map((choice, index) => {
        if (choice.content !== choices[index].content) {
          requestUpdateChoice({
            id: choice.id,
            content: choice.content,
            format: FormatModelToNumber(TextFormatModel.HTML),
            isCorrect: choice.id === formChoices.correctChoice,
            questionId,
          });
        }
      });
      const previousCorrectChoice = choices.find((c) => c.isCorrect);
      if (formChoices.correctChoice !== previousCorrectChoice?.id) {
        const newCorrectChoice = formChoices.choices.find(
          (c) => c.id === formChoices.correctChoice
        );
        if (newCorrectChoice) {
          requestUpdateChoice({
            id: formChoices.correctChoice,
            content: newCorrectChoice?.content,
            format: FormatModelToNumber(TextFormatModel.HTML),
            isCorrect: true,
            questionId,
          });
        }
        if (previousCorrectChoice) {
          requestUpdateChoice({
            ...previousCorrectChoice,
            isCorrect: false,
          });
        }
      }
    }, 3000);
    return () => clearTimeout(timeout);
  }, [formChoices, choices, questionId]);

  return (
    <Form onValuesChange={onValuesChange} form={choicesForm}>
      <Form.Item name={'correctChoice'}>
        <Radio.Group value={choices?.find((choice) => choice.isCorrect)?.id}>
          <Col flex="100%">
            <Title level={4} style={{ fontWeight: 'bold' }}>
              Choices
            </Title>
            {choices?.map((choice, index) => (
              <Col key={index}>
                <Radio value={choice.id}>
                  <Col>isCorrect</Col>
                </Radio>
                <Form.Item
                  name={['choices', index, 'content']}
                  initialValue={choice.content}
                  shouldUpdate
                >
                  <ReactQuill
                    style={{ width: '100%' }}
                    modules={modules}
                    formats={formats}
                  />
                </Form.Item>
                <Form.Item
                  name={['choices', index, 'id']}
                  initialValue={choice.id}
                >
                  <input type="hidden" />
                </Form.Item>
              </Col>
            ))}
          </Col>
        </Radio.Group>
      </Form.Item>
    </Form>
  );
}
