import React, { useEffect } from 'react';
import { useParams } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { notification } from 'antd';
import TabBarWrapper from './TabBarWrapper';
import { AppDispatch, RootState } from '../store';
import ExamPage from '../views/ExamPage';
import ExamCreate from '../views/ExamCreate';
import { removeActiveExam, setError } from '../../shared/actions/ExamAction';
import { requestGetExam } from '../ipcRenderer/senders/examSender';

export default function ExamWrapper(): JSX.Element {
  const { id } = useParams<{ id: string }>();
  const idInt = parseInt(id, 10);
  const activeExam = useSelector(
    (state: RootState) => state.ExamReducer.activeExam
  );
  const examError = useSelector((state: RootState) => state.ExamReducer.error);
  const activeTab = useSelector(
    (state: RootState) => state.TabReducer.activeTab
  );

  const dispatch = useDispatch<AppDispatch>();

  useEffect(() => {
    if (activeTab?.newExamTab === false) {
      requestGetExam(idInt);
    } else {
      dispatch(removeActiveExam());
    }
  }, [activeTab, dispatch, idInt]);
  useEffect(() => {
    if (examError) {
      notification.error({
        message: 'ERROR',
        description: `${examError.code}: ${examError.details}`,
      });
      dispatch(setError({ err: undefined }));
    }
  }, [examError, dispatch]);

  return (
    <TabBarWrapper>{activeExam ? <ExamPage /> : <ExamCreate />}</TabBarWrapper>
  );
}
