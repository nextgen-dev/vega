import React, { useState } from 'react';
import { Button, Popover, Row, Space, Typography } from 'antd';
import {
  DeleteFilled,
  LeftOutlined,
  PlusOutlined,
  RightOutlined,
  SaveFilled,
} from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../store';
import { changeQuestion } from '../../shared/actions/QuestionAction';
import {
  requestCreateQuestion,
  requestDeleteQuestion,
} from '../ipcRenderer/senders/questionSender';
import { createEmptyQuestion } from '../../shared/libs/templates';
import { LoadingType } from '../../shared/models/LoadingType';

export default function QuestionEditControl(props: {
  num: number;
  saveQuestion: () => void;
}): JSX.Element {
  const { Title } = Typography;
  const { num, saveQuestion } = props;

  const questions = useSelector(
    (state: RootState) => state.QuestionReducer.questions
  );
  const activeQuestionIdx = useSelector(
    (state: RootState) => state.QuestionReducer.activeQuestionIdx
  );
  const examId = useSelector(
    (state: RootState) => state.ExamReducer.activeExam?.id
  );
  const activeQuestion = useSelector(
    (state: RootState) => state.QuestionReducer.activeQuestion
  );
  const questionLoading = useSelector(
    (state: RootState) => state.QuestionReducer.loading
  );
  const choiceLoading = useSelector(
    (state: RootState) => state.ChoiceReducer.loading
  );

  const dispatch = useDispatch<AppDispatch>();
  const incrementIdx = (inc: number) => {
    const targetIndex = activeQuestionIdx + inc;
    dispatch(changeQuestion({ index: targetIndex }));
  };

  const createQuestion = () => {
    if (examId) {
      requestCreateQuestion(createEmptyQuestion(examId));
    }
  };

  const [popupVisible, setPopupVisible] = useState(false);
  const deleteQuestion = () => {
    if (activeQuestion) {
      requestDeleteQuestion(activeQuestion.id);
    }
    setPopupVisible(false);
  };
  const deletePopupContent = () => (
    <div>
      <Button onClick={deleteQuestion} type="primary" danger size="small">
        YES
      </Button>
    </div>
  );

  return (
    <Row align="middle" justify="space-between">
      <Space>
        <Button
          type="primary"
          style={{
            width: '3em',
            height: '3em',
            textAlign: 'center',
            padding: 0,
            background: '#0642CD',
            borderColor: '#0642CD',
          }}
          onClick={() => incrementIdx(-1)}
          disabled={
            activeQuestionIdx === 0 ||
            questionLoading != LoadingType.Idle ||
            choiceLoading != LoadingType.Idle
          }
        >
          <LeftOutlined />
        </Button>
        <Button
          type="primary"
          style={{
            width: '3em',
            height: '3em',
            textAlign: 'center',
            padding: 0,
            background: '#10C277',
            borderColor: '#10C277',
          }}
          onClick={() => createQuestion()}
          disabled={
            questionLoading != LoadingType.Idle ||
            choiceLoading != LoadingType.Idle
          }
        >
          <PlusOutlined />
        </Button>
        <Button
          type="primary"
          style={{
            width: '3em',
            height: '3em',
            textAlign: 'center',
            padding: 0,
            background: '#0642CD',
            borderColor: '#0642CD',
          }}
          onClick={() => incrementIdx(1)}
          disabled={
            activeQuestionIdx === questions.length - 1 ||
            questionLoading != LoadingType.Idle ||
            choiceLoading != LoadingType.Idle
          }
        >
          <RightOutlined />
        </Button>
      </Space>
      <Title level={3}>{`Number ${num}`}</Title>
      <Space>
        <Button
          type="primary"
          style={{
            width: '3em',
            height: '3em',
            textAlign: 'center',
            padding: 0,
            background: '#10C277',
            borderColor: '#10C277',
          }}
          disabled={
            questionLoading != LoadingType.Idle ||
            choiceLoading != LoadingType.Idle
          }
          onClick={saveQuestion}
        >
          <SaveFilled />
        </Button>
        <Popover
          content={deletePopupContent()}
          title="Are you sure you want to delete this question?"
          trigger="click"
          visible={popupVisible}
          onVisibleChange={setPopupVisible}
          placement="bottomRight"
        >
          <Button
            type="primary"
            danger
            style={{
              width: '3em',
              height: '3em',
              textAlign: 'center',
              padding: 0,
            }}
            disabled={
              questions.length === 1 ||
              questionLoading != LoadingType.Idle ||
              choiceLoading != LoadingType.Idle
            }
          >
            <DeleteFilled />
          </Button>
        </Popover>
      </Space>
    </Row>
  );
}
