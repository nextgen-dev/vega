import React from 'react';
import { Button, Col, Row } from 'antd';
import { DeleteFilled, EditFilled } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import AttemptModel from '../../shared/models/AttemptModel';
import { LoadingType } from '../../shared/models/LoadingType';

export default function AttemptInfo(props: {
  attempt: AttemptModel;
  toggleEdit: () => void;
}): JSX.Element {
  const { attempt, toggleEdit } = props;

  const loading = useSelector(
    (state: RootState) => state.AttemptReducer.loading
  );

  const deleteAttempt = () => {
    console.log(attempt.id);
  };

  return (
    <Row>
      <Col span={3}>
        <p>{attempt.participantId}</p>
      </Col>
      <Col span={3}>
        <p>{attempt.finished ? 'Finished' : 'Not Finished'}</p>
      </Col>
      <Col span={2}>
        <p>{attempt.corrects}</p>
      </Col>
      <Col span={3}>
        <p>{attempt.wrongs}</p>
      </Col>
      <Col span={3}>
        <p>{attempt.unanswered}</p>
      </Col>
      <Col span={4}>
        <p>{new Date(attempt.createdAt || '').toLocaleString()}</p>
      </Col>
      <Col span={4}>
        <p>{new Date(attempt.updatedAt || '').toLocaleString()}</p>
      </Col>
      <Col span={2}>
        <Row justify="end">
          <Col>
            <Button
              type="primary"
              size="small"
              onClick={toggleEdit}
              disabled={loading !== LoadingType.Idle}
            >
              <EditFilled style={{ color: 'white' }} />
            </Button>{' '}
          </Col>
          <Col>
            <Button
              type="primary"
              danger
              size="small"
              style={{ marginLeft: '0.5rem' }}
              onClick={deleteAttempt}
              disabled={loading !== LoadingType.Idle}
            >
              <DeleteFilled style={{ color: 'white' }} />
            </Button>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}
