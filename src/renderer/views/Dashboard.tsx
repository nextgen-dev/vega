import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { notification, Row } from 'antd';
import TabBarWrapper from '../components/TabBarWrapper';
import { AppDispatch, RootState } from '../store';
import ExamList from '../components/ExamList';
import { removeActiveExam } from '../../shared/actions/ExamAction';
import { requestListExams } from '../ipcRenderer/senders/examSender';
import { setError } from '../../shared/actions/ExamAction';

export default function Dashboard(): JSX.Element {
  const examList = useSelector((state: RootState) => state.ExamReducer.exams);
  const pageNumber = useSelector((state: RootState) => state.ExamReducer.page);
  const examError = useSelector((state: RootState) => state.ExamReducer.error);

  const dispatch = useDispatch<AppDispatch>();

  dispatch(removeActiveExam());
  useEffect(() => {
    requestListExams(10, pageNumber);
    if (examError) {
      notification.error({
        message: 'ERROR',
        description: `${examError.code}: ${examError.details}`,
      });
      dispatch(setError({ err: undefined }));
    }
  }, [examError, pageNumber, dispatch]);

  return (
    <TabBarWrapper>
      <Row
        align="middle"
        justify="center"
        style={{ height: '100%', padding: 48 }}
      >
        {examList ? <ExamList examList={examList} /> : ''}
      </Row>
    </TabBarWrapper>
  );
}
