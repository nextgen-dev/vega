import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import { rendererStore } from './store';

render(
  <Provider store={rendererStore}>
    <App />
  </Provider>,
  document.getElementById('root')
);
