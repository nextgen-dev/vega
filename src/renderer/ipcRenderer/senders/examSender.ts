import { rendererStore } from '../../store';
import {
  createExam,
  deleteExam,
  getExam,
  listExams,
  updateExam,
} from '../../../shared/actions/ExamAction';
import { CreateExamRequest, Exam } from '../../../main/services/protobuf/gen/testycool/exam';

export const requestListExams = (size: number, page: number) => {
  rendererStore.dispatch(listExams.pending());
  window.exam.ipcRenderer.listExams(size, page);
};

export const requestGetExam = (id: number) => {
  rendererStore.dispatch(getExam.pending());
  window.exam.ipcRenderer.getExam(id);
};

export const requestCreateExam = (newExam: CreateExamRequest) => {
  rendererStore.dispatch(createExam.pending());
  window.exam.ipcRenderer.createExam(newExam);
};

export const requestUpdateExam = (updatedExam: Exam) => {
  rendererStore.dispatch(updateExam.pending());
  window.exam.ipcRenderer.updateExam(updatedExam);
};

export const requestDeleteExam = (id: number) => {
  rendererStore.dispatch(deleteExam.pending());
  window.exam.ipcRenderer.deleteExam(id);
};
