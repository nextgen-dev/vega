import { rendererStore } from '../../store';
import {
  CreateParticipantRequest,
  Participant,
} from '../../../main/services/protobuf/gen/testycool/participant';
import {
  createParticipant,
  deleteParticipant,
  listParticipants,
  updateParticipant,
} from '../../../shared/actions/ParticipantAction';

export function requestListParticipants(
  size: number,
  page: number,
  examId: number
) {
  rendererStore.dispatch(listParticipants.pending());
  window.participant.ipcRenderer.listParticipants(size, page, examId);
}

export const requestCreateParticipant = (
  newParticipant: CreateParticipantRequest
) => {
  rendererStore.dispatch(createParticipant.pending());
  window.participant.ipcRenderer.createParticipant(newParticipant);
};

export const requestUpdateParticipant = (updatedParticipant: Participant) => {
  rendererStore.dispatch(updateParticipant.pending());
  window.participant.ipcRenderer.updateParticipant(updatedParticipant);
};

export const requestDeleteParticipant = (id: number) => {
  rendererStore.dispatch(deleteParticipant.pending());
  window.participant.ipcRenderer.deleteParticipant(id);
};
