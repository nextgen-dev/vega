import { rendererStore } from '../../store';
import {
  createQuestion,
  deleteQuestion,
  getQuestion,
  listQuestions,
  updateQuestion,
} from '../../../shared/actions/QuestionAction';
import { QuestionModel } from '../../../shared/models/QuestionModel';
import { CreateQuestionRequest } from '../../../main/services/protobuf/gen/testycool/question';

export const requestListQuestions = (
  size: number,
  page: number,
  examId: number
) => {
  rendererStore.dispatch(listQuestions.pending());
  window.question.ipcRenderer.listQuestions(size, page, examId);
};

export const requestGetQuestion = (id: number) => {
  rendererStore.dispatch(getQuestion.pending());
  window.question.ipcRenderer.getQuestion(id);
};

export const requestCreateQuestion = (newQuestion: CreateQuestionRequest) => {
  rendererStore.dispatch(createQuestion.pending());
  window.question.ipcRenderer.createQuestion(newQuestion);
};

export const requestUpdateQuestion = (updatedQuestion: QuestionModel) => {
  rendererStore.dispatch(updateQuestion.pending());
  window.question.ipcRenderer.updateQuestion(updatedQuestion);
};

export const requestUpdateQuestionType = (updatedQuestion: QuestionModel) => {
  rendererStore.dispatch(updateQuestion.pending());
  window.question.ipcRenderer.updateQuestionType(updatedQuestion);
};

export const requestDeleteQuestion = (id: number) => {
  rendererStore.dispatch(deleteQuestion.pending());
  window.question.ipcRenderer.deleteQuestion(id);
};
