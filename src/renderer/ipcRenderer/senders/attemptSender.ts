import attemptAction from '../../../shared/actions/AttemptAction';
import { GetAttemptRequestDto } from '../../../shared/dto/GetAttemptRequestDto';
import { UpdateAttemptRequestDto } from '../../../shared/dto/UpdateAttemptRequestDto';
import { rendererStore } from '../../store';
import { ListAttemptsRequestDto } from '../../../shared/dto/ListAttemptsRequestDto';

const attemptSender = {
  listAttempts: (dto: ListAttemptsRequestDto) => {
    rendererStore.dispatch(attemptAction.getAttempt.pending());
    window.attempt.ipcRenderer.listAttempts(dto);
  },
  getAttempt: (dto: GetAttemptRequestDto) => {
    rendererStore.dispatch(attemptAction.getAttempt.pending());
    window.attempt.ipcRenderer.getAttempt(dto);
  },
  updateAttempt: (dto: UpdateAttemptRequestDto) => {
    rendererStore.dispatch(attemptAction.updateAttempt.pending());
    window.attempt.ipcRenderer.updateAttempt(dto);
  },
};

export default attemptSender;
