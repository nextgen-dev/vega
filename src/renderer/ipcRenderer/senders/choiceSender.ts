import { rendererStore } from '../../store';
import ChoiceModel from '../../../shared/models/ChoiceModel';
import {
  createChoice,
  deleteChoice,
  listChoices,
  updateChoice,
} from '../../../shared/actions/ChoiceAction';
import { CreateChoiceRequest } from '../../../main/services/protobuf/gen/testycool/choice';

export const requestListChoices = (
  size: number,
  page: number,
  questionId: number
) => {
  rendererStore.dispatch(listChoices.pending());
  window.choice.ipcRenderer.listChoices(size, page, questionId);
};

export const requestCreateChoice = (newChoice: CreateChoiceRequest) => {
  rendererStore.dispatch(createChoice.pending());
  window.choice.ipcRenderer.createChoice(newChoice);
};

export const requestUpdateChoice = (updatedChoice: ChoiceModel) => {
  rendererStore.dispatch(updateChoice.pending());
  window.choice.ipcRenderer.updateChoice(updatedChoice);
};

export const requestDeleteChoice = (id: number) => {
  rendererStore.dispatch(deleteChoice.pending());
  window.choice.ipcRenderer.deleteChoice(id);
};
