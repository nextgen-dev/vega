import { GetAttemptResponseDto } from '../../../shared/dto/GetAttemptResponseDto';
import attemptAction from '../../../shared/actions/AttemptAction';
import { rendererStore } from '../../store';
import ErrorResponseDto from '../../../shared/dto/ErrorResponseDto';
import { UpdateAttemptResponseDto } from '../../../shared/dto/UpdateAttemptResponseDto';
import { ListAttemptsResponseDto } from '../../../shared/dto/ListAttemptsResponseDto';

const attemptListener = () => {
  window.attempt.ipcRenderer.on(
    'list-attempts',
    (args: ListAttemptsResponseDto) => {
      const parsedAttemptList = args.attempts?.map((attempt) => {
        return {
          ...attempt,
          createdAt: new Date(attempt.createdAt || '').toISOString(),
          updatedAt: new Date(attempt.updatedAt || '').toISOString(),
        };
      });
      rendererStore.dispatch(
        attemptAction.listAttempts.fulfilled({ attempts: parsedAttemptList })
      );
    }
  );
  window.attempt.ipcRenderer.on(
    'get-attempt',
    (args: GetAttemptResponseDto) => {
      if (args.attempt) {
        rendererStore.dispatch(
          attemptAction.getAttempt.fulfilled({
            attempt: {
              ...args.attempt,
              createdAt: new Date(args.attempt?.createdAt || '').toISOString(),
              updatedAt: new Date(args.attempt?.updatedAt || '').toISOString(),
            },
          })
        );
      }
    }
  );
  window.attempt.ipcRenderer.on(
    'update-attempt',
    (args: UpdateAttemptResponseDto) => {
      if (args.attempt) {
        rendererStore.dispatch(
          attemptAction.updateAttempt.fulfilled({
            attempt: {
              ...args.attempt,
              createdAt: new Date(args.attempt?.createdAt || '').toISOString(),
              updatedAt: new Date(args.attempt?.updatedAt || '').toISOString(),
            },
          })
        );
      }
    }
  );
  window.attempt.ipcRenderer.on('attempt-error', (error: ErrorResponseDto) => {
    rendererStore.dispatch(attemptAction.setError(error));
  });
};

export default attemptListener;
