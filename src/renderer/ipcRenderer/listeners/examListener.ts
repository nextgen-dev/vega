import {
  CreateExamResponse,
  GetExamResponse,
  ListExamsResponse,
  UpdateExamResponse,
} from '../../../main/services/protobuf/gen/testycool/exam';
import { StatusNumberToStatusModel } from '../../../shared/libs/StatusParser';
import { rendererStore } from '../../store';
import {
  createExam,
  deleteExam,
  getExam,
  listExams,
  setError,
} from '../../../shared/actions/ExamAction';
import { updateTab } from '../../../shared/actions/TabAction';

export default function examListener() {
  window.exam.ipcRenderer.on('list-exams', (args: ListExamsResponse) => {
    const parsedExamList = args.exams.map((exam) => {
      return {
        ...exam,
        startAt: exam.startAt?.toISOString(),
        status: StatusNumberToStatusModel(exam.status),
      };
    });
    rendererStore.dispatch(
      listExams.fulfilled({
        ...args,
        exams: parsedExamList,
      })
    );
    rendererStore.dispatch(setError({ err: undefined }));
  });
  window.exam.ipcRenderer.on('get-exam', (args: GetExamResponse) => {
    if (args.exam) {
      rendererStore.dispatch(
        getExam.fulfilled({
          exam: {
            ...args.exam,
            startAt: args.exam?.startAt?.toISOString(),
            status: StatusNumberToStatusModel(args.exam?.status || 0),
          },
        })
      );
    }
  });
  window.exam.ipcRenderer.on('create-exam', (args: CreateExamResponse) => {
    if (args.exam) {
      rendererStore.dispatch(
        createExam.fulfilled({
          exam: {
            ...args.exam,
            startAt: args.exam.startAt?.toISOString(),
            status: StatusNumberToStatusModel(args.exam.status),
          },
        })
      );
      rendererStore.dispatch(
        updateTab({ id: args.exam.id, title: args.exam.title })
      );
    }
  });
  window.exam.ipcRenderer.on('update-exam', (args: UpdateExamResponse) => {
    if (args.exam) {
      rendererStore.dispatch(
        createExam.fulfilled({
          exam: {
            ...args.exam,
            startAt: args.exam.startAt?.toISOString(),
            status: StatusNumberToStatusModel(args.exam.status),
          },
        })
      );
    }
  });
  window.exam.ipcRenderer.on('delete-exam', (args: { id: number }) => {
    rendererStore.dispatch(deleteExam.fulfilled({ id: args.id }));
  });
  window.exam.ipcRenderer.on(
    'exam-error',
    (args: { code: number; details: string }) => {
      rendererStore.dispatch(setError({ err: args }));
    }
  );
}
