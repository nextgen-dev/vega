import { rendererStore } from '../../store';
import {
  CreateChoiceResponse,
  Choice,
} from '../../../main/services/protobuf/gen/testycool/choice';
import {
  createChoice,
  deleteChoice,
  listChoices,
  updateChoice,
  setError, clearChoices
} from '../../../shared/actions/ChoiceAction';
import { NumberToFormatModel } from '../../../shared/libs/TextFormatTypeParser';
import { createEmptyChoice } from '../../../shared/libs/templates';
import { requestCreateChoice } from '../senders/choiceSender';

export default function choiceListener() {
  window.choice.ipcRenderer.on(
    'list-choices',
    (response: {
      size: number;
      page: number;
      choices: Choice[];
      totalSize: number;
      questionId: number;
    }) => {
      if (response.size === 0) {
        rendererStore.dispatch(clearChoices());
        for (let i = 0; i < 5; i++) {
          requestCreateChoice(createEmptyChoice(response.questionId));
        }
        return;
      }

      rendererStore.dispatch(
        listChoices.fulfilled({
          ...response,
          choices: response.choices.map((choice) => ({
            ...choice,
            format: NumberToFormatModel(choice.format),
          })),
        })
      );
    }
  );
  window.choice.ipcRenderer.on(
    'create-choice',
    (response: CreateChoiceResponse) => {
      if (response.choice) {
        rendererStore.dispatch(
          createChoice.fulfilled({
            choice: {
              ...response.choice,
              format: NumberToFormatModel(response.choice.format),
            },
          })
        );
      }
    }
  );
  window.choice.ipcRenderer.on(
    'update-choice',
    (response: CreateChoiceResponse) => {
      if (response.choice) {
        rendererStore.dispatch(
          updateChoice.fulfilled({
            choice: {
              ...response.choice,
              format: NumberToFormatModel(response.choice.format),
            },
          })
        );
      }
    }
  );
  window.choice.ipcRenderer.on('delete-choice', (response: { id: number }) => {
    rendererStore.dispatch(deleteChoice.fulfilled({ id: response.id }));
  });
  window.choice.ipcRenderer.on(
    'choice-error',
    (response: { code: number; details: string }) => {
      rendererStore.dispatch(setError({ err: response }));
    }
  );
}
