/* eslint-disable */
import Long from 'long';
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from '@grpc/grpc-js';
import _m0 from 'protobufjs/minimal';
import {
  TextFormat,
  textFormatFromJSON,
  textFormatToJSON,
} from '../testycool/text_format';
import { Empty } from '../google/protobuf/empty';

export const protobufPackage = 'testycool.v1';

export interface GetAnswerRequest {
  id: number;
}

export interface GetAnswerResponse {
  answer: Answer | undefined;
}

export interface ListAnswersRequest {
  size: number;
  page: number;
  filter: ListAnswersRequest_Filter | undefined;
}

export interface ListAnswersRequest_Filter {
  participantId?: number | undefined;
  examId?: number | undefined;
}

export interface ListAnswersResponse {
  answers: Answer[];
  size: number;
  page: number;
  totalSize: number;
}

export interface CreateAnswerRequest {
  participantId: number;
  questionId: number;
  choiceId: number | undefined;
  essay: EssayAnswer | undefined;
}

export interface CreateAnswerResponse {
  answer: Answer | undefined;
}

export interface UpdateAnswerRequest {
  answer: Answer | undefined;
}

export interface UpdateAnswerResponse {
  answer: Answer | undefined;
}

export interface DeleteAnswerRequest {
  id: number;
}

export interface Answer {
  id: number;
  participantId: number;
  questionId: number;
  choiceId: number | undefined;
  essay: EssayAnswer | undefined;
  isCorrect: boolean;
}

export interface EssayAnswer {
  format: TextFormat;
  content: string;
}

const baseGetAnswerRequest: object = { id: 0 };

export const GetAnswerRequest = {
  encode(
    message: GetAnswerRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetAnswerRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetAnswerRequest } as GetAnswerRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAnswerRequest {
    const message = { ...baseGetAnswerRequest } as GetAnswerRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: GetAnswerRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<GetAnswerRequest>): GetAnswerRequest {
    const message = { ...baseGetAnswerRequest } as GetAnswerRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseGetAnswerResponse: object = {};

export const GetAnswerResponse = {
  encode(
    message: GetAnswerResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.answer !== undefined) {
      Answer.encode(message.answer, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetAnswerResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetAnswerResponse } as GetAnswerResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.answer = Answer.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAnswerResponse {
    const message = { ...baseGetAnswerResponse } as GetAnswerResponse;
    if (object.answer !== undefined && object.answer !== null) {
      message.answer = Answer.fromJSON(object.answer);
    } else {
      message.answer = undefined;
    }
    return message;
  },

  toJSON(message: GetAnswerResponse): unknown {
    const obj: any = {};
    message.answer !== undefined &&
      (obj.answer = message.answer ? Answer.toJSON(message.answer) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<GetAnswerResponse>): GetAnswerResponse {
    const message = { ...baseGetAnswerResponse } as GetAnswerResponse;
    if (object.answer !== undefined && object.answer !== null) {
      message.answer = Answer.fromPartial(object.answer);
    } else {
      message.answer = undefined;
    }
    return message;
  },
};

const baseListAnswersRequest: object = { size: 0, page: 0 };

export const ListAnswersRequest = {
  encode(
    message: ListAnswersRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.size !== 0) {
      writer.uint32(8).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(16).int32(message.page);
    }
    if (message.filter !== undefined) {
      ListAnswersRequest_Filter.encode(
        message.filter,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListAnswersRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseListAnswersRequest } as ListAnswersRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.size = reader.int32();
          break;
        case 2:
          message.page = reader.int32();
          break;
        case 3:
          message.filter = ListAnswersRequest_Filter.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAnswersRequest {
    const message = { ...baseListAnswersRequest } as ListAnswersRequest;
    if (object.size !== undefined && object.size !== null) {
      message.size = Number(object.size);
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = Number(object.page);
    } else {
      message.page = 0;
    }
    if (object.filter !== undefined && object.filter !== null) {
      message.filter = ListAnswersRequest_Filter.fromJSON(object.filter);
    } else {
      message.filter = undefined;
    }
    return message;
  },

  toJSON(message: ListAnswersRequest): unknown {
    const obj: any = {};
    message.size !== undefined && (obj.size = message.size);
    message.page !== undefined && (obj.page = message.page);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? ListAnswersRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<ListAnswersRequest>): ListAnswersRequest {
    const message = { ...baseListAnswersRequest } as ListAnswersRequest;
    if (object.size !== undefined && object.size !== null) {
      message.size = object.size;
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = object.page;
    } else {
      message.page = 0;
    }
    if (object.filter !== undefined && object.filter !== null) {
      message.filter = ListAnswersRequest_Filter.fromPartial(object.filter);
    } else {
      message.filter = undefined;
    }
    return message;
  },
};

const baseListAnswersRequest_Filter: object = {};

export const ListAnswersRequest_Filter = {
  encode(
    message: ListAnswersRequest_Filter,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participantId !== undefined) {
      writer.uint32(16).int32(message.participantId);
    }
    if (message.examId !== undefined) {
      writer.uint32(24).int32(message.examId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListAnswersRequest_Filter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseListAnswersRequest_Filter,
    } as ListAnswersRequest_Filter;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 2:
          message.participantId = reader.int32();
          break;
        case 3:
          message.examId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAnswersRequest_Filter {
    const message = {
      ...baseListAnswersRequest_Filter,
    } as ListAnswersRequest_Filter;
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = Number(object.participantId);
    } else {
      message.participantId = undefined;
    }
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = Number(object.examId);
    } else {
      message.examId = undefined;
    }
    return message;
  },

  toJSON(message: ListAnswersRequest_Filter): unknown {
    const obj: any = {};
    message.participantId !== undefined &&
      (obj.participantId = message.participantId);
    message.examId !== undefined && (obj.examId = message.examId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ListAnswersRequest_Filter>
  ): ListAnswersRequest_Filter {
    const message = {
      ...baseListAnswersRequest_Filter,
    } as ListAnswersRequest_Filter;
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = object.participantId;
    } else {
      message.participantId = undefined;
    }
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = object.examId;
    } else {
      message.examId = undefined;
    }
    return message;
  },
};

const baseListAnswersResponse: object = { size: 0, page: 0, totalSize: 0 };

export const ListAnswersResponse = {
  encode(
    message: ListAnswersResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.answers) {
      Answer.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.size !== 0) {
      writer.uint32(16).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(24).int32(message.page);
    }
    if (message.totalSize !== 0) {
      writer.uint32(32).int32(message.totalSize);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListAnswersResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseListAnswersResponse } as ListAnswersResponse;
    message.answers = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.answers.push(Answer.decode(reader, reader.uint32()));
          break;
        case 2:
          message.size = reader.int32();
          break;
        case 3:
          message.page = reader.int32();
          break;
        case 4:
          message.totalSize = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAnswersResponse {
    const message = { ...baseListAnswersResponse } as ListAnswersResponse;
    message.answers = [];
    if (object.answers !== undefined && object.answers !== null) {
      for (const e of object.answers) {
        message.answers.push(Answer.fromJSON(e));
      }
    }
    if (object.size !== undefined && object.size !== null) {
      message.size = Number(object.size);
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = Number(object.page);
    } else {
      message.page = 0;
    }
    if (object.totalSize !== undefined && object.totalSize !== null) {
      message.totalSize = Number(object.totalSize);
    } else {
      message.totalSize = 0;
    }
    return message;
  },

  toJSON(message: ListAnswersResponse): unknown {
    const obj: any = {};
    if (message.answers) {
      obj.answers = message.answers.map((e) =>
        e ? Answer.toJSON(e) : undefined
      );
    } else {
      obj.answers = [];
    }
    message.size !== undefined && (obj.size = message.size);
    message.page !== undefined && (obj.page = message.page);
    message.totalSize !== undefined && (obj.totalSize = message.totalSize);
    return obj;
  },

  fromPartial(object: DeepPartial<ListAnswersResponse>): ListAnswersResponse {
    const message = { ...baseListAnswersResponse } as ListAnswersResponse;
    message.answers = [];
    if (object.answers !== undefined && object.answers !== null) {
      for (const e of object.answers) {
        message.answers.push(Answer.fromPartial(e));
      }
    }
    if (object.size !== undefined && object.size !== null) {
      message.size = object.size;
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = object.page;
    } else {
      message.page = 0;
    }
    if (object.totalSize !== undefined && object.totalSize !== null) {
      message.totalSize = object.totalSize;
    } else {
      message.totalSize = 0;
    }
    return message;
  },
};

const baseCreateAnswerRequest: object = { participantId: 0, questionId: 0 };

export const CreateAnswerRequest = {
  encode(
    message: CreateAnswerRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participantId !== 0) {
      writer.uint32(8).int32(message.participantId);
    }
    if (message.questionId !== 0) {
      writer.uint32(16).int32(message.questionId);
    }
    if (message.choiceId !== undefined) {
      writer.uint32(24).int32(message.choiceId);
    }
    if (message.essay !== undefined) {
      EssayAnswer.encode(message.essay, writer.uint32(34).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CreateAnswerRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCreateAnswerRequest } as CreateAnswerRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participantId = reader.int32();
          break;
        case 2:
          message.questionId = reader.int32();
          break;
        case 3:
          message.choiceId = reader.int32();
          break;
        case 4:
          message.essay = EssayAnswer.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateAnswerRequest {
    const message = { ...baseCreateAnswerRequest } as CreateAnswerRequest;
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = Number(object.participantId);
    } else {
      message.participantId = 0;
    }
    if (object.questionId !== undefined && object.questionId !== null) {
      message.questionId = Number(object.questionId);
    } else {
      message.questionId = 0;
    }
    if (object.choiceId !== undefined && object.choiceId !== null) {
      message.choiceId = Number(object.choiceId);
    } else {
      message.choiceId = undefined;
    }
    if (object.essay !== undefined && object.essay !== null) {
      message.essay = EssayAnswer.fromJSON(object.essay);
    } else {
      message.essay = undefined;
    }
    return message;
  },

  toJSON(message: CreateAnswerRequest): unknown {
    const obj: any = {};
    message.participantId !== undefined &&
      (obj.participantId = message.participantId);
    message.questionId !== undefined && (obj.questionId = message.questionId);
    message.choiceId !== undefined && (obj.choiceId = message.choiceId);
    message.essay !== undefined &&
      (obj.essay = message.essay
        ? EssayAnswer.toJSON(message.essay)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<CreateAnswerRequest>): CreateAnswerRequest {
    const message = { ...baseCreateAnswerRequest } as CreateAnswerRequest;
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = object.participantId;
    } else {
      message.participantId = 0;
    }
    if (object.questionId !== undefined && object.questionId !== null) {
      message.questionId = object.questionId;
    } else {
      message.questionId = 0;
    }
    if (object.choiceId !== undefined && object.choiceId !== null) {
      message.choiceId = object.choiceId;
    } else {
      message.choiceId = undefined;
    }
    if (object.essay !== undefined && object.essay !== null) {
      message.essay = EssayAnswer.fromPartial(object.essay);
    } else {
      message.essay = undefined;
    }
    return message;
  },
};

const baseCreateAnswerResponse: object = {};

export const CreateAnswerResponse = {
  encode(
    message: CreateAnswerResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.answer !== undefined) {
      Answer.encode(message.answer, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateAnswerResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCreateAnswerResponse } as CreateAnswerResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.answer = Answer.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateAnswerResponse {
    const message = { ...baseCreateAnswerResponse } as CreateAnswerResponse;
    if (object.answer !== undefined && object.answer !== null) {
      message.answer = Answer.fromJSON(object.answer);
    } else {
      message.answer = undefined;
    }
    return message;
  },

  toJSON(message: CreateAnswerResponse): unknown {
    const obj: any = {};
    message.answer !== undefined &&
      (obj.answer = message.answer ? Answer.toJSON(message.answer) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<CreateAnswerResponse>): CreateAnswerResponse {
    const message = { ...baseCreateAnswerResponse } as CreateAnswerResponse;
    if (object.answer !== undefined && object.answer !== null) {
      message.answer = Answer.fromPartial(object.answer);
    } else {
      message.answer = undefined;
    }
    return message;
  },
};

const baseUpdateAnswerRequest: object = {};

export const UpdateAnswerRequest = {
  encode(
    message: UpdateAnswerRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.answer !== undefined) {
      Answer.encode(message.answer, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UpdateAnswerRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUpdateAnswerRequest } as UpdateAnswerRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.answer = Answer.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateAnswerRequest {
    const message = { ...baseUpdateAnswerRequest } as UpdateAnswerRequest;
    if (object.answer !== undefined && object.answer !== null) {
      message.answer = Answer.fromJSON(object.answer);
    } else {
      message.answer = undefined;
    }
    return message;
  },

  toJSON(message: UpdateAnswerRequest): unknown {
    const obj: any = {};
    message.answer !== undefined &&
      (obj.answer = message.answer ? Answer.toJSON(message.answer) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<UpdateAnswerRequest>): UpdateAnswerRequest {
    const message = { ...baseUpdateAnswerRequest } as UpdateAnswerRequest;
    if (object.answer !== undefined && object.answer !== null) {
      message.answer = Answer.fromPartial(object.answer);
    } else {
      message.answer = undefined;
    }
    return message;
  },
};

const baseUpdateAnswerResponse: object = {};

export const UpdateAnswerResponse = {
  encode(
    message: UpdateAnswerResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.answer !== undefined) {
      Answer.encode(message.answer, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateAnswerResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUpdateAnswerResponse } as UpdateAnswerResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.answer = Answer.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateAnswerResponse {
    const message = { ...baseUpdateAnswerResponse } as UpdateAnswerResponse;
    if (object.answer !== undefined && object.answer !== null) {
      message.answer = Answer.fromJSON(object.answer);
    } else {
      message.answer = undefined;
    }
    return message;
  },

  toJSON(message: UpdateAnswerResponse): unknown {
    const obj: any = {};
    message.answer !== undefined &&
      (obj.answer = message.answer ? Answer.toJSON(message.answer) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<UpdateAnswerResponse>): UpdateAnswerResponse {
    const message = { ...baseUpdateAnswerResponse } as UpdateAnswerResponse;
    if (object.answer !== undefined && object.answer !== null) {
      message.answer = Answer.fromPartial(object.answer);
    } else {
      message.answer = undefined;
    }
    return message;
  },
};

const baseDeleteAnswerRequest: object = { id: 0 };

export const DeleteAnswerRequest = {
  encode(
    message: DeleteAnswerRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DeleteAnswerRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseDeleteAnswerRequest } as DeleteAnswerRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DeleteAnswerRequest {
    const message = { ...baseDeleteAnswerRequest } as DeleteAnswerRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: DeleteAnswerRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<DeleteAnswerRequest>): DeleteAnswerRequest {
    const message = { ...baseDeleteAnswerRequest } as DeleteAnswerRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseAnswer: object = {
  id: 0,
  participantId: 0,
  questionId: 0,
  isCorrect: false,
};

export const Answer = {
  encode(
    message: Answer,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.participantId !== 0) {
      writer.uint32(16).int32(message.participantId);
    }
    if (message.questionId !== 0) {
      writer.uint32(24).int32(message.questionId);
    }
    if (message.choiceId !== undefined) {
      writer.uint32(32).int32(message.choiceId);
    }
    if (message.essay !== undefined) {
      EssayAnswer.encode(message.essay, writer.uint32(42).fork()).ldelim();
    }
    if (message.isCorrect === true) {
      writer.uint32(48).bool(message.isCorrect);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Answer {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAnswer } as Answer;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.participantId = reader.int32();
          break;
        case 3:
          message.questionId = reader.int32();
          break;
        case 4:
          message.choiceId = reader.int32();
          break;
        case 5:
          message.essay = EssayAnswer.decode(reader, reader.uint32());
          break;
        case 6:
          message.isCorrect = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Answer {
    const message = { ...baseAnswer } as Answer;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = Number(object.participantId);
    } else {
      message.participantId = 0;
    }
    if (object.questionId !== undefined && object.questionId !== null) {
      message.questionId = Number(object.questionId);
    } else {
      message.questionId = 0;
    }
    if (object.choiceId !== undefined && object.choiceId !== null) {
      message.choiceId = Number(object.choiceId);
    } else {
      message.choiceId = undefined;
    }
    if (object.essay !== undefined && object.essay !== null) {
      message.essay = EssayAnswer.fromJSON(object.essay);
    } else {
      message.essay = undefined;
    }
    if (object.isCorrect !== undefined && object.isCorrect !== null) {
      message.isCorrect = Boolean(object.isCorrect);
    } else {
      message.isCorrect = false;
    }
    return message;
  },

  toJSON(message: Answer): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.participantId !== undefined &&
      (obj.participantId = message.participantId);
    message.questionId !== undefined && (obj.questionId = message.questionId);
    message.choiceId !== undefined && (obj.choiceId = message.choiceId);
    message.essay !== undefined &&
      (obj.essay = message.essay
        ? EssayAnswer.toJSON(message.essay)
        : undefined);
    message.isCorrect !== undefined && (obj.isCorrect = message.isCorrect);
    return obj;
  },

  fromPartial(object: DeepPartial<Answer>): Answer {
    const message = { ...baseAnswer } as Answer;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = object.participantId;
    } else {
      message.participantId = 0;
    }
    if (object.questionId !== undefined && object.questionId !== null) {
      message.questionId = object.questionId;
    } else {
      message.questionId = 0;
    }
    if (object.choiceId !== undefined && object.choiceId !== null) {
      message.choiceId = object.choiceId;
    } else {
      message.choiceId = undefined;
    }
    if (object.essay !== undefined && object.essay !== null) {
      message.essay = EssayAnswer.fromPartial(object.essay);
    } else {
      message.essay = undefined;
    }
    if (object.isCorrect !== undefined && object.isCorrect !== null) {
      message.isCorrect = object.isCorrect;
    } else {
      message.isCorrect = false;
    }
    return message;
  },
};

const baseEssayAnswer: object = { format: 0, content: '' };

export const EssayAnswer = {
  encode(
    message: EssayAnswer,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.format !== 0) {
      writer.uint32(32).int32(message.format);
    }
    if (message.content !== '') {
      writer.uint32(42).string(message.content);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): EssayAnswer {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseEssayAnswer } as EssayAnswer;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 4:
          message.format = reader.int32() as any;
          break;
        case 5:
          message.content = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): EssayAnswer {
    const message = { ...baseEssayAnswer } as EssayAnswer;
    if (object.format !== undefined && object.format !== null) {
      message.format = textFormatFromJSON(object.format);
    } else {
      message.format = 0;
    }
    if (object.content !== undefined && object.content !== null) {
      message.content = String(object.content);
    } else {
      message.content = '';
    }
    return message;
  },

  toJSON(message: EssayAnswer): unknown {
    const obj: any = {};
    message.format !== undefined &&
      (obj.format = textFormatToJSON(message.format));
    message.content !== undefined && (obj.content = message.content);
    return obj;
  },

  fromPartial(object: DeepPartial<EssayAnswer>): EssayAnswer {
    const message = { ...baseEssayAnswer } as EssayAnswer;
    if (object.format !== undefined && object.format !== null) {
      message.format = object.format;
    } else {
      message.format = 0;
    }
    if (object.content !== undefined && object.content !== null) {
      message.content = object.content;
    } else {
      message.content = '';
    }
    return message;
  },
};

export const AnswerServiceService = {
  getAnswer: {
    path: '/testycool.v1.AnswerService/GetAnswer',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetAnswerRequest) =>
      Buffer.from(GetAnswerRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetAnswerRequest.decode(value),
    responseSerialize: (value: GetAnswerResponse) =>
      Buffer.from(GetAnswerResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => GetAnswerResponse.decode(value),
  },
  listAnswers: {
    path: '/testycool.v1.AnswerService/ListAnswers',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ListAnswersRequest) =>
      Buffer.from(ListAnswersRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => ListAnswersRequest.decode(value),
    responseSerialize: (value: ListAnswersResponse) =>
      Buffer.from(ListAnswersResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => ListAnswersResponse.decode(value),
  },
  createAnswer: {
    path: '/testycool.v1.AnswerService/CreateAnswer',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CreateAnswerRequest) =>
      Buffer.from(CreateAnswerRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => CreateAnswerRequest.decode(value),
    responseSerialize: (value: CreateAnswerResponse) =>
      Buffer.from(CreateAnswerResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => CreateAnswerResponse.decode(value),
  },
  updateAnswer: {
    path: '/testycool.v1.AnswerService/UpdateAnswer',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: UpdateAnswerRequest) =>
      Buffer.from(UpdateAnswerRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => UpdateAnswerRequest.decode(value),
    responseSerialize: (value: UpdateAnswerResponse) =>
      Buffer.from(UpdateAnswerResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => UpdateAnswerResponse.decode(value),
  },
  deleteAnswer: {
    path: '/testycool.v1.AnswerService/DeleteAnswer',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: DeleteAnswerRequest) =>
      Buffer.from(DeleteAnswerRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => DeleteAnswerRequest.decode(value),
    responseSerialize: (value: Empty) =>
      Buffer.from(Empty.encode(value).finish()),
    responseDeserialize: (value: Buffer) => Empty.decode(value),
  },
} as const;

export interface AnswerServiceServer extends UntypedServiceImplementation {
  getAnswer: handleUnaryCall<GetAnswerRequest, GetAnswerResponse>;
  listAnswers: handleUnaryCall<ListAnswersRequest, ListAnswersResponse>;
  createAnswer: handleUnaryCall<CreateAnswerRequest, CreateAnswerResponse>;
  updateAnswer: handleUnaryCall<UpdateAnswerRequest, UpdateAnswerResponse>;
  deleteAnswer: handleUnaryCall<DeleteAnswerRequest, Empty>;
}

export interface AnswerServiceClient extends Client {
  getAnswer(
    request: GetAnswerRequest,
    callback: (error: ServiceError | null, response: GetAnswerResponse) => void
  ): ClientUnaryCall;
  getAnswer(
    request: GetAnswerRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: GetAnswerResponse) => void
  ): ClientUnaryCall;
  getAnswer(
    request: GetAnswerRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: GetAnswerResponse) => void
  ): ClientUnaryCall;
  listAnswers(
    request: ListAnswersRequest,
    callback: (
      error: ServiceError | null,
      response: ListAnswersResponse
    ) => void
  ): ClientUnaryCall;
  listAnswers(
    request: ListAnswersRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ListAnswersResponse
    ) => void
  ): ClientUnaryCall;
  listAnswers(
    request: ListAnswersRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ListAnswersResponse
    ) => void
  ): ClientUnaryCall;
  createAnswer(
    request: CreateAnswerRequest,
    callback: (
      error: ServiceError | null,
      response: CreateAnswerResponse
    ) => void
  ): ClientUnaryCall;
  createAnswer(
    request: CreateAnswerRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CreateAnswerResponse
    ) => void
  ): ClientUnaryCall;
  createAnswer(
    request: CreateAnswerRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CreateAnswerResponse
    ) => void
  ): ClientUnaryCall;
  updateAnswer(
    request: UpdateAnswerRequest,
    callback: (
      error: ServiceError | null,
      response: UpdateAnswerResponse
    ) => void
  ): ClientUnaryCall;
  updateAnswer(
    request: UpdateAnswerRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: UpdateAnswerResponse
    ) => void
  ): ClientUnaryCall;
  updateAnswer(
    request: UpdateAnswerRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: UpdateAnswerResponse
    ) => void
  ): ClientUnaryCall;
  deleteAnswer(
    request: DeleteAnswerRequest,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteAnswer(
    request: DeleteAnswerRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteAnswer(
    request: DeleteAnswerRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
}

export const AnswerServiceClient = makeGenericClientConstructor(
  AnswerServiceService,
  'testycool.v1.AnswerService'
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): AnswerServiceClient;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
