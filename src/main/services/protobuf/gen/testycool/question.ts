/* eslint-disable */
import Long from 'long';
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from '@grpc/grpc-js';
import _m0 from 'protobufjs/minimal';
import {
  TextFormat,
  textFormatFromJSON,
  textFormatToJSON,
} from '../testycool/text_format';
import { Empty } from '../google/protobuf/empty';

export const protobufPackage = 'testycool.v1';

export interface GetQuestionRequest {
  id: number;
}

export interface GetQuestionResponse {
  question: Question | undefined;
}

export interface ListQuestionsRequest {
  size: number;
  page: number;
  filter: ListQuestionsRequest_Filter | undefined;
}

export interface ListQuestionsRequest_Filter {
  examId?: number | undefined;
}

export interface ListQuestionsResponse {
  questions: Question[];
  size: number;
  page: number;
  totalSize: number;
}

export interface CreateQuestionRequest {
  examId: number;
  type: Question_Type;
  format: TextFormat;
  content: string;
}

export interface CreateQuestionResponse {
  question: Question | undefined;
}

export interface UpdateQuestionRequest {
  question: Question | undefined;
}

export interface UpdateQuestionResponse {
  question: Question | undefined;
}

export interface DeleteQuestionRequest {
  id: number;
}

export interface Question {
  id: number;
  examId: number;
  type: Question_Type;
  format: TextFormat;
  content: string;
}

export enum Question_Type {
  UNKNOWN = 0,
  MULTIPLE_CHOICE = 1,
  ESSAY = 2,
}

export function question_TypeFromJSON(object: any): Question_Type {
  switch (object) {
    case 0:
    case 'UNKNOWN':
      return Question_Type.UNKNOWN;
    case 1:
    case 'MULTIPLE_CHOICE':
      return Question_Type.MULTIPLE_CHOICE;
    case 2:
    case 'ESSAY':
      return Question_Type.ESSAY;
    default:
      throw new globalThis.Error(
        'Unrecognized enum value ' + object + ' for enum Question_Type'
      );
  }
}

export function question_TypeToJSON(object: Question_Type): string {
  switch (object) {
    case Question_Type.UNKNOWN:
      return 'UNKNOWN';
    case Question_Type.MULTIPLE_CHOICE:
      return 'MULTIPLE_CHOICE';
    case Question_Type.ESSAY:
      return 'ESSAY';
    default:
      return 'UNKNOWN';
  }
}

const baseGetQuestionRequest: object = { id: 0 };

export const GetQuestionRequest = {
  encode(
    message: GetQuestionRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetQuestionRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetQuestionRequest } as GetQuestionRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetQuestionRequest {
    const message = { ...baseGetQuestionRequest } as GetQuestionRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: GetQuestionRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<GetQuestionRequest>): GetQuestionRequest {
    const message = { ...baseGetQuestionRequest } as GetQuestionRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseGetQuestionResponse: object = {};

export const GetQuestionResponse = {
  encode(
    message: GetQuestionResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.question !== undefined) {
      Question.encode(message.question, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetQuestionResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetQuestionResponse } as GetQuestionResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.question = Question.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetQuestionResponse {
    const message = { ...baseGetQuestionResponse } as GetQuestionResponse;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromJSON(object.question);
    } else {
      message.question = undefined;
    }
    return message;
  },

  toJSON(message: GetQuestionResponse): unknown {
    const obj: any = {};
    message.question !== undefined &&
      (obj.question = message.question
        ? Question.toJSON(message.question)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<GetQuestionResponse>): GetQuestionResponse {
    const message = { ...baseGetQuestionResponse } as GetQuestionResponse;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromPartial(object.question);
    } else {
      message.question = undefined;
    }
    return message;
  },
};

const baseListQuestionsRequest: object = { size: 0, page: 0 };

export const ListQuestionsRequest = {
  encode(
    message: ListQuestionsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.size !== 0) {
      writer.uint32(8).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(16).int32(message.page);
    }
    if (message.filter !== undefined) {
      ListQuestionsRequest_Filter.encode(
        message.filter,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListQuestionsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseListQuestionsRequest } as ListQuestionsRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.size = reader.int32();
          break;
        case 2:
          message.page = reader.int32();
          break;
        case 3:
          message.filter = ListQuestionsRequest_Filter.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListQuestionsRequest {
    const message = { ...baseListQuestionsRequest } as ListQuestionsRequest;
    if (object.size !== undefined && object.size !== null) {
      message.size = Number(object.size);
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = Number(object.page);
    } else {
      message.page = 0;
    }
    if (object.filter !== undefined && object.filter !== null) {
      message.filter = ListQuestionsRequest_Filter.fromJSON(object.filter);
    } else {
      message.filter = undefined;
    }
    return message;
  },

  toJSON(message: ListQuestionsRequest): unknown {
    const obj: any = {};
    message.size !== undefined && (obj.size = message.size);
    message.page !== undefined && (obj.page = message.page);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? ListQuestionsRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<ListQuestionsRequest>): ListQuestionsRequest {
    const message = { ...baseListQuestionsRequest } as ListQuestionsRequest;
    if (object.size !== undefined && object.size !== null) {
      message.size = object.size;
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = object.page;
    } else {
      message.page = 0;
    }
    if (object.filter !== undefined && object.filter !== null) {
      message.filter = ListQuestionsRequest_Filter.fromPartial(object.filter);
    } else {
      message.filter = undefined;
    }
    return message;
  },
};

const baseListQuestionsRequest_Filter: object = {};

export const ListQuestionsRequest_Filter = {
  encode(
    message: ListQuestionsRequest_Filter,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examId !== undefined) {
      writer.uint32(8).int32(message.examId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListQuestionsRequest_Filter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseListQuestionsRequest_Filter,
    } as ListQuestionsRequest_Filter;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.examId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListQuestionsRequest_Filter {
    const message = {
      ...baseListQuestionsRequest_Filter,
    } as ListQuestionsRequest_Filter;
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = Number(object.examId);
    } else {
      message.examId = undefined;
    }
    return message;
  },

  toJSON(message: ListQuestionsRequest_Filter): unknown {
    const obj: any = {};
    message.examId !== undefined && (obj.examId = message.examId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ListQuestionsRequest_Filter>
  ): ListQuestionsRequest_Filter {
    const message = {
      ...baseListQuestionsRequest_Filter,
    } as ListQuestionsRequest_Filter;
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = object.examId;
    } else {
      message.examId = undefined;
    }
    return message;
  },
};

const baseListQuestionsResponse: object = { size: 0, page: 0, totalSize: 0 };

export const ListQuestionsResponse = {
  encode(
    message: ListQuestionsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.questions) {
      Question.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.size !== 0) {
      writer.uint32(16).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(24).int32(message.page);
    }
    if (message.totalSize !== 0) {
      writer.uint32(32).int32(message.totalSize);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListQuestionsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseListQuestionsResponse } as ListQuestionsResponse;
    message.questions = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.questions.push(Question.decode(reader, reader.uint32()));
          break;
        case 2:
          message.size = reader.int32();
          break;
        case 3:
          message.page = reader.int32();
          break;
        case 4:
          message.totalSize = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListQuestionsResponse {
    const message = { ...baseListQuestionsResponse } as ListQuestionsResponse;
    message.questions = [];
    if (object.questions !== undefined && object.questions !== null) {
      for (const e of object.questions) {
        message.questions.push(Question.fromJSON(e));
      }
    }
    if (object.size !== undefined && object.size !== null) {
      message.size = Number(object.size);
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = Number(object.page);
    } else {
      message.page = 0;
    }
    if (object.totalSize !== undefined && object.totalSize !== null) {
      message.totalSize = Number(object.totalSize);
    } else {
      message.totalSize = 0;
    }
    return message;
  },

  toJSON(message: ListQuestionsResponse): unknown {
    const obj: any = {};
    if (message.questions) {
      obj.questions = message.questions.map((e) =>
        e ? Question.toJSON(e) : undefined
      );
    } else {
      obj.questions = [];
    }
    message.size !== undefined && (obj.size = message.size);
    message.page !== undefined && (obj.page = message.page);
    message.totalSize !== undefined && (obj.totalSize = message.totalSize);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ListQuestionsResponse>
  ): ListQuestionsResponse {
    const message = { ...baseListQuestionsResponse } as ListQuestionsResponse;
    message.questions = [];
    if (object.questions !== undefined && object.questions !== null) {
      for (const e of object.questions) {
        message.questions.push(Question.fromPartial(e));
      }
    }
    if (object.size !== undefined && object.size !== null) {
      message.size = object.size;
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = object.page;
    } else {
      message.page = 0;
    }
    if (object.totalSize !== undefined && object.totalSize !== null) {
      message.totalSize = object.totalSize;
    } else {
      message.totalSize = 0;
    }
    return message;
  },
};

const baseCreateQuestionRequest: object = {
  examId: 0,
  type: 0,
  format: 0,
  content: '',
};

export const CreateQuestionRequest = {
  encode(
    message: CreateQuestionRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examId !== 0) {
      writer.uint32(8).int32(message.examId);
    }
    if (message.type !== 0) {
      writer.uint32(16).int32(message.type);
    }
    if (message.format !== 0) {
      writer.uint32(24).int32(message.format);
    }
    if (message.content !== '') {
      writer.uint32(34).string(message.content);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateQuestionRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCreateQuestionRequest } as CreateQuestionRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.examId = reader.int32();
          break;
        case 2:
          message.type = reader.int32() as any;
          break;
        case 3:
          message.format = reader.int32() as any;
          break;
        case 4:
          message.content = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateQuestionRequest {
    const message = { ...baseCreateQuestionRequest } as CreateQuestionRequest;
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = Number(object.examId);
    } else {
      message.examId = 0;
    }
    if (object.type !== undefined && object.type !== null) {
      message.type = question_TypeFromJSON(object.type);
    } else {
      message.type = 0;
    }
    if (object.format !== undefined && object.format !== null) {
      message.format = textFormatFromJSON(object.format);
    } else {
      message.format = 0;
    }
    if (object.content !== undefined && object.content !== null) {
      message.content = String(object.content);
    } else {
      message.content = '';
    }
    return message;
  },

  toJSON(message: CreateQuestionRequest): unknown {
    const obj: any = {};
    message.examId !== undefined && (obj.examId = message.examId);
    message.type !== undefined &&
      (obj.type = question_TypeToJSON(message.type));
    message.format !== undefined &&
      (obj.format = textFormatToJSON(message.format));
    message.content !== undefined && (obj.content = message.content);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CreateQuestionRequest>
  ): CreateQuestionRequest {
    const message = { ...baseCreateQuestionRequest } as CreateQuestionRequest;
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = object.examId;
    } else {
      message.examId = 0;
    }
    if (object.type !== undefined && object.type !== null) {
      message.type = object.type;
    } else {
      message.type = 0;
    }
    if (object.format !== undefined && object.format !== null) {
      message.format = object.format;
    } else {
      message.format = 0;
    }
    if (object.content !== undefined && object.content !== null) {
      message.content = object.content;
    } else {
      message.content = '';
    }
    return message;
  },
};

const baseCreateQuestionResponse: object = {};

export const CreateQuestionResponse = {
  encode(
    message: CreateQuestionResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.question !== undefined) {
      Question.encode(message.question, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateQuestionResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCreateQuestionResponse } as CreateQuestionResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.question = Question.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateQuestionResponse {
    const message = { ...baseCreateQuestionResponse } as CreateQuestionResponse;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromJSON(object.question);
    } else {
      message.question = undefined;
    }
    return message;
  },

  toJSON(message: CreateQuestionResponse): unknown {
    const obj: any = {};
    message.question !== undefined &&
      (obj.question = message.question
        ? Question.toJSON(message.question)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CreateQuestionResponse>
  ): CreateQuestionResponse {
    const message = { ...baseCreateQuestionResponse } as CreateQuestionResponse;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromPartial(object.question);
    } else {
      message.question = undefined;
    }
    return message;
  },
};

const baseUpdateQuestionRequest: object = {};

export const UpdateQuestionRequest = {
  encode(
    message: UpdateQuestionRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.question !== undefined) {
      Question.encode(message.question, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateQuestionRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUpdateQuestionRequest } as UpdateQuestionRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.question = Question.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateQuestionRequest {
    const message = { ...baseUpdateQuestionRequest } as UpdateQuestionRequest;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromJSON(object.question);
    } else {
      message.question = undefined;
    }
    return message;
  },

  toJSON(message: UpdateQuestionRequest): unknown {
    const obj: any = {};
    message.question !== undefined &&
      (obj.question = message.question
        ? Question.toJSON(message.question)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<UpdateQuestionRequest>
  ): UpdateQuestionRequest {
    const message = { ...baseUpdateQuestionRequest } as UpdateQuestionRequest;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromPartial(object.question);
    } else {
      message.question = undefined;
    }
    return message;
  },
};

const baseUpdateQuestionResponse: object = {};

export const UpdateQuestionResponse = {
  encode(
    message: UpdateQuestionResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.question !== undefined) {
      Question.encode(message.question, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateQuestionResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUpdateQuestionResponse } as UpdateQuestionResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.question = Question.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateQuestionResponse {
    const message = { ...baseUpdateQuestionResponse } as UpdateQuestionResponse;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromJSON(object.question);
    } else {
      message.question = undefined;
    }
    return message;
  },

  toJSON(message: UpdateQuestionResponse): unknown {
    const obj: any = {};
    message.question !== undefined &&
      (obj.question = message.question
        ? Question.toJSON(message.question)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<UpdateQuestionResponse>
  ): UpdateQuestionResponse {
    const message = { ...baseUpdateQuestionResponse } as UpdateQuestionResponse;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromPartial(object.question);
    } else {
      message.question = undefined;
    }
    return message;
  },
};

const baseDeleteQuestionRequest: object = { id: 0 };

export const DeleteQuestionRequest = {
  encode(
    message: DeleteQuestionRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): DeleteQuestionRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseDeleteQuestionRequest } as DeleteQuestionRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DeleteQuestionRequest {
    const message = { ...baseDeleteQuestionRequest } as DeleteQuestionRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: DeleteQuestionRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<DeleteQuestionRequest>
  ): DeleteQuestionRequest {
    const message = { ...baseDeleteQuestionRequest } as DeleteQuestionRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseQuestion: object = {
  id: 0,
  examId: 0,
  type: 0,
  format: 0,
  content: '',
};

export const Question = {
  encode(
    message: Question,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.examId !== 0) {
      writer.uint32(16).int32(message.examId);
    }
    if (message.type !== 0) {
      writer.uint32(24).int32(message.type);
    }
    if (message.format !== 0) {
      writer.uint32(32).int32(message.format);
    }
    if (message.content !== '') {
      writer.uint32(42).string(message.content);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Question {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQuestion } as Question;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.examId = reader.int32();
          break;
        case 3:
          message.type = reader.int32() as any;
          break;
        case 4:
          message.format = reader.int32() as any;
          break;
        case 5:
          message.content = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Question {
    const message = { ...baseQuestion } as Question;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = Number(object.examId);
    } else {
      message.examId = 0;
    }
    if (object.type !== undefined && object.type !== null) {
      message.type = question_TypeFromJSON(object.type);
    } else {
      message.type = 0;
    }
    if (object.format !== undefined && object.format !== null) {
      message.format = textFormatFromJSON(object.format);
    } else {
      message.format = 0;
    }
    if (object.content !== undefined && object.content !== null) {
      message.content = String(object.content);
    } else {
      message.content = '';
    }
    return message;
  },

  toJSON(message: Question): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.examId !== undefined && (obj.examId = message.examId);
    message.type !== undefined &&
      (obj.type = question_TypeToJSON(message.type));
    message.format !== undefined &&
      (obj.format = textFormatToJSON(message.format));
    message.content !== undefined && (obj.content = message.content);
    return obj;
  },

  fromPartial(object: DeepPartial<Question>): Question {
    const message = { ...baseQuestion } as Question;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = object.examId;
    } else {
      message.examId = 0;
    }
    if (object.type !== undefined && object.type !== null) {
      message.type = object.type;
    } else {
      message.type = 0;
    }
    if (object.format !== undefined && object.format !== null) {
      message.format = object.format;
    } else {
      message.format = 0;
    }
    if (object.content !== undefined && object.content !== null) {
      message.content = object.content;
    } else {
      message.content = '';
    }
    return message;
  },
};

export const QuestionServiceService = {
  getQuestion: {
    path: '/testycool.v1.QuestionService/GetQuestion',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetQuestionRequest) =>
      Buffer.from(GetQuestionRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetQuestionRequest.decode(value),
    responseSerialize: (value: GetQuestionResponse) =>
      Buffer.from(GetQuestionResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => GetQuestionResponse.decode(value),
  },
  listQuestions: {
    path: '/testycool.v1.QuestionService/ListQuestions',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ListQuestionsRequest) =>
      Buffer.from(ListQuestionsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => ListQuestionsRequest.decode(value),
    responseSerialize: (value: ListQuestionsResponse) =>
      Buffer.from(ListQuestionsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => ListQuestionsResponse.decode(value),
  },
  createQuestion: {
    path: '/testycool.v1.QuestionService/CreateQuestion',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CreateQuestionRequest) =>
      Buffer.from(CreateQuestionRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => CreateQuestionRequest.decode(value),
    responseSerialize: (value: CreateQuestionResponse) =>
      Buffer.from(CreateQuestionResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      CreateQuestionResponse.decode(value),
  },
  updateQuestion: {
    path: '/testycool.v1.QuestionService/UpdateQuestion',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: UpdateQuestionRequest) =>
      Buffer.from(UpdateQuestionRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => UpdateQuestionRequest.decode(value),
    responseSerialize: (value: UpdateQuestionResponse) =>
      Buffer.from(UpdateQuestionResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      UpdateQuestionResponse.decode(value),
  },
  deleteQuestion: {
    path: '/testycool.v1.QuestionService/DeleteQuestion',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: DeleteQuestionRequest) =>
      Buffer.from(DeleteQuestionRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => DeleteQuestionRequest.decode(value),
    responseSerialize: (value: Empty) =>
      Buffer.from(Empty.encode(value).finish()),
    responseDeserialize: (value: Buffer) => Empty.decode(value),
  },
} as const;

export interface QuestionServiceServer extends UntypedServiceImplementation {
  getQuestion: handleUnaryCall<GetQuestionRequest, GetQuestionResponse>;
  listQuestions: handleUnaryCall<ListQuestionsRequest, ListQuestionsResponse>;
  createQuestion: handleUnaryCall<
    CreateQuestionRequest,
    CreateQuestionResponse
  >;
  updateQuestion: handleUnaryCall<
    UpdateQuestionRequest,
    UpdateQuestionResponse
  >;
  deleteQuestion: handleUnaryCall<DeleteQuestionRequest, Empty>;
}

export interface QuestionServiceClient extends Client {
  getQuestion(
    request: GetQuestionRequest,
    callback: (
      error: ServiceError | null,
      response: GetQuestionResponse
    ) => void
  ): ClientUnaryCall;
  getQuestion(
    request: GetQuestionRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: GetQuestionResponse
    ) => void
  ): ClientUnaryCall;
  getQuestion(
    request: GetQuestionRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: GetQuestionResponse
    ) => void
  ): ClientUnaryCall;
  listQuestions(
    request: ListQuestionsRequest,
    callback: (
      error: ServiceError | null,
      response: ListQuestionsResponse
    ) => void
  ): ClientUnaryCall;
  listQuestions(
    request: ListQuestionsRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ListQuestionsResponse
    ) => void
  ): ClientUnaryCall;
  listQuestions(
    request: ListQuestionsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ListQuestionsResponse
    ) => void
  ): ClientUnaryCall;
  createQuestion(
    request: CreateQuestionRequest,
    callback: (
      error: ServiceError | null,
      response: CreateQuestionResponse
    ) => void
  ): ClientUnaryCall;
  createQuestion(
    request: CreateQuestionRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CreateQuestionResponse
    ) => void
  ): ClientUnaryCall;
  createQuestion(
    request: CreateQuestionRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CreateQuestionResponse
    ) => void
  ): ClientUnaryCall;
  updateQuestion(
    request: UpdateQuestionRequest,
    callback: (
      error: ServiceError | null,
      response: UpdateQuestionResponse
    ) => void
  ): ClientUnaryCall;
  updateQuestion(
    request: UpdateQuestionRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: UpdateQuestionResponse
    ) => void
  ): ClientUnaryCall;
  updateQuestion(
    request: UpdateQuestionRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: UpdateQuestionResponse
    ) => void
  ): ClientUnaryCall;
  deleteQuestion(
    request: DeleteQuestionRequest,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteQuestion(
    request: DeleteQuestionRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteQuestion(
    request: DeleteQuestionRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
}

export const QuestionServiceClient = makeGenericClientConstructor(
  QuestionServiceService,
  'testycool.v1.QuestionService'
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): QuestionServiceClient;
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
