/* eslint-disable */
import Long from 'long';
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from '@grpc/grpc-js';
import _m0 from 'protobufjs/minimal';
import {
  TextFormat,
  textFormatFromJSON,
  textFormatToJSON,
} from '../testycool/text_format';
import { Empty } from '../google/protobuf/empty';

export const protobufPackage = 'testycool.v1';

export interface GetChoiceRequest {
  id: number;
}

export interface GetChoiceResponse {
  choice: Choice | undefined;
}

export interface ListChoicesRequest {
  size: number;
  page: number;
  filter: ListChoicesRequest_Filter | undefined;
}

export interface ListChoicesRequest_Filter {
  questionId?: number | undefined;
}

export interface ListChoicesResponse {
  choices: Choice[];
  size: number;
  page: number;
  totalSize: number;
}

export interface CreateChoiceRequest {
  questionId: number;
  isCorrect: boolean;
  format: TextFormat;
  content: string;
}

export interface CreateChoiceResponse {
  choice: Choice | undefined;
}

export interface UpdateChoiceRequest {
  choice: Choice | undefined;
}

export interface UpdateChoiceResponse {
  choice: Choice | undefined;
}

export interface DeleteChoiceRequest {
  id: number;
}

export interface Choice {
  id: number;
  questionId: number;
  isCorrect: boolean;
  format: TextFormat;
  content: string;
}

const baseGetChoiceRequest: object = { id: 0 };

export const GetChoiceRequest = {
  encode(
    message: GetChoiceRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetChoiceRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetChoiceRequest } as GetChoiceRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetChoiceRequest {
    const message = { ...baseGetChoiceRequest } as GetChoiceRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: GetChoiceRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<GetChoiceRequest>): GetChoiceRequest {
    const message = { ...baseGetChoiceRequest } as GetChoiceRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseGetChoiceResponse: object = {};

export const GetChoiceResponse = {
  encode(
    message: GetChoiceResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.choice !== undefined) {
      Choice.encode(message.choice, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetChoiceResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetChoiceResponse } as GetChoiceResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.choice = Choice.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetChoiceResponse {
    const message = { ...baseGetChoiceResponse } as GetChoiceResponse;
    if (object.choice !== undefined && object.choice !== null) {
      message.choice = Choice.fromJSON(object.choice);
    } else {
      message.choice = undefined;
    }
    return message;
  },

  toJSON(message: GetChoiceResponse): unknown {
    const obj: any = {};
    message.choice !== undefined &&
      (obj.choice = message.choice ? Choice.toJSON(message.choice) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<GetChoiceResponse>): GetChoiceResponse {
    const message = { ...baseGetChoiceResponse } as GetChoiceResponse;
    if (object.choice !== undefined && object.choice !== null) {
      message.choice = Choice.fromPartial(object.choice);
    } else {
      message.choice = undefined;
    }
    return message;
  },
};

const baseListChoicesRequest: object = { size: 0, page: 0 };

export const ListChoicesRequest = {
  encode(
    message: ListChoicesRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.size !== 0) {
      writer.uint32(8).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(16).int32(message.page);
    }
    if (message.filter !== undefined) {
      ListChoicesRequest_Filter.encode(
        message.filter,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListChoicesRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseListChoicesRequest } as ListChoicesRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.size = reader.int32();
          break;
        case 2:
          message.page = reader.int32();
          break;
        case 3:
          message.filter = ListChoicesRequest_Filter.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListChoicesRequest {
    const message = { ...baseListChoicesRequest } as ListChoicesRequest;
    if (object.size !== undefined && object.size !== null) {
      message.size = Number(object.size);
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = Number(object.page);
    } else {
      message.page = 0;
    }
    if (object.filter !== undefined && object.filter !== null) {
      message.filter = ListChoicesRequest_Filter.fromJSON(object.filter);
    } else {
      message.filter = undefined;
    }
    return message;
  },

  toJSON(message: ListChoicesRequest): unknown {
    const obj: any = {};
    message.size !== undefined && (obj.size = message.size);
    message.page !== undefined && (obj.page = message.page);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? ListChoicesRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<ListChoicesRequest>): ListChoicesRequest {
    const message = { ...baseListChoicesRequest } as ListChoicesRequest;
    if (object.size !== undefined && object.size !== null) {
      message.size = object.size;
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = object.page;
    } else {
      message.page = 0;
    }
    if (object.filter !== undefined && object.filter !== null) {
      message.filter = ListChoicesRequest_Filter.fromPartial(object.filter);
    } else {
      message.filter = undefined;
    }
    return message;
  },
};

const baseListChoicesRequest_Filter: object = {};

export const ListChoicesRequest_Filter = {
  encode(
    message: ListChoicesRequest_Filter,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.questionId !== undefined) {
      writer.uint32(8).int32(message.questionId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListChoicesRequest_Filter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseListChoicesRequest_Filter,
    } as ListChoicesRequest_Filter;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.questionId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListChoicesRequest_Filter {
    const message = {
      ...baseListChoicesRequest_Filter,
    } as ListChoicesRequest_Filter;
    if (object.questionId !== undefined && object.questionId !== null) {
      message.questionId = Number(object.questionId);
    } else {
      message.questionId = undefined;
    }
    return message;
  },

  toJSON(message: ListChoicesRequest_Filter): unknown {
    const obj: any = {};
    message.questionId !== undefined && (obj.questionId = message.questionId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ListChoicesRequest_Filter>
  ): ListChoicesRequest_Filter {
    const message = {
      ...baseListChoicesRequest_Filter,
    } as ListChoicesRequest_Filter;
    if (object.questionId !== undefined && object.questionId !== null) {
      message.questionId = object.questionId;
    } else {
      message.questionId = undefined;
    }
    return message;
  },
};

const baseListChoicesResponse: object = { size: 0, page: 0, totalSize: 0 };

export const ListChoicesResponse = {
  encode(
    message: ListChoicesResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.choices) {
      Choice.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.size !== 0) {
      writer.uint32(16).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(24).int32(message.page);
    }
    if (message.totalSize !== 0) {
      writer.uint32(32).int32(message.totalSize);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListChoicesResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseListChoicesResponse } as ListChoicesResponse;
    message.choices = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.choices.push(Choice.decode(reader, reader.uint32()));
          break;
        case 2:
          message.size = reader.int32();
          break;
        case 3:
          message.page = reader.int32();
          break;
        case 4:
          message.totalSize = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListChoicesResponse {
    const message = { ...baseListChoicesResponse } as ListChoicesResponse;
    message.choices = [];
    if (object.choices !== undefined && object.choices !== null) {
      for (const e of object.choices) {
        message.choices.push(Choice.fromJSON(e));
      }
    }
    if (object.size !== undefined && object.size !== null) {
      message.size = Number(object.size);
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = Number(object.page);
    } else {
      message.page = 0;
    }
    if (object.totalSize !== undefined && object.totalSize !== null) {
      message.totalSize = Number(object.totalSize);
    } else {
      message.totalSize = 0;
    }
    return message;
  },

  toJSON(message: ListChoicesResponse): unknown {
    const obj: any = {};
    if (message.choices) {
      obj.choices = message.choices.map((e) =>
        e ? Choice.toJSON(e) : undefined
      );
    } else {
      obj.choices = [];
    }
    message.size !== undefined && (obj.size = message.size);
    message.page !== undefined && (obj.page = message.page);
    message.totalSize !== undefined && (obj.totalSize = message.totalSize);
    return obj;
  },

  fromPartial(object: DeepPartial<ListChoicesResponse>): ListChoicesResponse {
    const message = { ...baseListChoicesResponse } as ListChoicesResponse;
    message.choices = [];
    if (object.choices !== undefined && object.choices !== null) {
      for (const e of object.choices) {
        message.choices.push(Choice.fromPartial(e));
      }
    }
    if (object.size !== undefined && object.size !== null) {
      message.size = object.size;
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = object.page;
    } else {
      message.page = 0;
    }
    if (object.totalSize !== undefined && object.totalSize !== null) {
      message.totalSize = object.totalSize;
    } else {
      message.totalSize = 0;
    }
    return message;
  },
};

const baseCreateChoiceRequest: object = {
  questionId: 0,
  isCorrect: false,
  format: 0,
  content: '',
};

export const CreateChoiceRequest = {
  encode(
    message: CreateChoiceRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.questionId !== 0) {
      writer.uint32(8).int32(message.questionId);
    }
    if (message.isCorrect === true) {
      writer.uint32(16).bool(message.isCorrect);
    }
    if (message.format !== 0) {
      writer.uint32(24).int32(message.format);
    }
    if (message.content !== '') {
      writer.uint32(34).string(message.content);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CreateChoiceRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCreateChoiceRequest } as CreateChoiceRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.questionId = reader.int32();
          break;
        case 2:
          message.isCorrect = reader.bool();
          break;
        case 3:
          message.format = reader.int32() as any;
          break;
        case 4:
          message.content = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateChoiceRequest {
    const message = { ...baseCreateChoiceRequest } as CreateChoiceRequest;
    if (object.questionId !== undefined && object.questionId !== null) {
      message.questionId = Number(object.questionId);
    } else {
      message.questionId = 0;
    }
    if (object.isCorrect !== undefined && object.isCorrect !== null) {
      message.isCorrect = Boolean(object.isCorrect);
    } else {
      message.isCorrect = false;
    }
    if (object.format !== undefined && object.format !== null) {
      message.format = textFormatFromJSON(object.format);
    } else {
      message.format = 0;
    }
    if (object.content !== undefined && object.content !== null) {
      message.content = String(object.content);
    } else {
      message.content = '';
    }
    return message;
  },

  toJSON(message: CreateChoiceRequest): unknown {
    const obj: any = {};
    message.questionId !== undefined && (obj.questionId = message.questionId);
    message.isCorrect !== undefined && (obj.isCorrect = message.isCorrect);
    message.format !== undefined &&
      (obj.format = textFormatToJSON(message.format));
    message.content !== undefined && (obj.content = message.content);
    return obj;
  },

  fromPartial(object: DeepPartial<CreateChoiceRequest>): CreateChoiceRequest {
    const message = { ...baseCreateChoiceRequest } as CreateChoiceRequest;
    if (object.questionId !== undefined && object.questionId !== null) {
      message.questionId = object.questionId;
    } else {
      message.questionId = 0;
    }
    if (object.isCorrect !== undefined && object.isCorrect !== null) {
      message.isCorrect = object.isCorrect;
    } else {
      message.isCorrect = false;
    }
    if (object.format !== undefined && object.format !== null) {
      message.format = object.format;
    } else {
      message.format = 0;
    }
    if (object.content !== undefined && object.content !== null) {
      message.content = object.content;
    } else {
      message.content = '';
    }
    return message;
  },
};

const baseCreateChoiceResponse: object = {};

export const CreateChoiceResponse = {
  encode(
    message: CreateChoiceResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.choice !== undefined) {
      Choice.encode(message.choice, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateChoiceResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCreateChoiceResponse } as CreateChoiceResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.choice = Choice.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateChoiceResponse {
    const message = { ...baseCreateChoiceResponse } as CreateChoiceResponse;
    if (object.choice !== undefined && object.choice !== null) {
      message.choice = Choice.fromJSON(object.choice);
    } else {
      message.choice = undefined;
    }
    return message;
  },

  toJSON(message: CreateChoiceResponse): unknown {
    const obj: any = {};
    message.choice !== undefined &&
      (obj.choice = message.choice ? Choice.toJSON(message.choice) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<CreateChoiceResponse>): CreateChoiceResponse {
    const message = { ...baseCreateChoiceResponse } as CreateChoiceResponse;
    if (object.choice !== undefined && object.choice !== null) {
      message.choice = Choice.fromPartial(object.choice);
    } else {
      message.choice = undefined;
    }
    return message;
  },
};

const baseUpdateChoiceRequest: object = {};

export const UpdateChoiceRequest = {
  encode(
    message: UpdateChoiceRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.choice !== undefined) {
      Choice.encode(message.choice, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UpdateChoiceRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUpdateChoiceRequest } as UpdateChoiceRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.choice = Choice.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateChoiceRequest {
    const message = { ...baseUpdateChoiceRequest } as UpdateChoiceRequest;
    if (object.choice !== undefined && object.choice !== null) {
      message.choice = Choice.fromJSON(object.choice);
    } else {
      message.choice = undefined;
    }
    return message;
  },

  toJSON(message: UpdateChoiceRequest): unknown {
    const obj: any = {};
    message.choice !== undefined &&
      (obj.choice = message.choice ? Choice.toJSON(message.choice) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<UpdateChoiceRequest>): UpdateChoiceRequest {
    const message = { ...baseUpdateChoiceRequest } as UpdateChoiceRequest;
    if (object.choice !== undefined && object.choice !== null) {
      message.choice = Choice.fromPartial(object.choice);
    } else {
      message.choice = undefined;
    }
    return message;
  },
};

const baseUpdateChoiceResponse: object = {};

export const UpdateChoiceResponse = {
  encode(
    message: UpdateChoiceResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.choice !== undefined) {
      Choice.encode(message.choice, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateChoiceResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUpdateChoiceResponse } as UpdateChoiceResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.choice = Choice.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateChoiceResponse {
    const message = { ...baseUpdateChoiceResponse } as UpdateChoiceResponse;
    if (object.choice !== undefined && object.choice !== null) {
      message.choice = Choice.fromJSON(object.choice);
    } else {
      message.choice = undefined;
    }
    return message;
  },

  toJSON(message: UpdateChoiceResponse): unknown {
    const obj: any = {};
    message.choice !== undefined &&
      (obj.choice = message.choice ? Choice.toJSON(message.choice) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<UpdateChoiceResponse>): UpdateChoiceResponse {
    const message = { ...baseUpdateChoiceResponse } as UpdateChoiceResponse;
    if (object.choice !== undefined && object.choice !== null) {
      message.choice = Choice.fromPartial(object.choice);
    } else {
      message.choice = undefined;
    }
    return message;
  },
};

const baseDeleteChoiceRequest: object = { id: 0 };

export const DeleteChoiceRequest = {
  encode(
    message: DeleteChoiceRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DeleteChoiceRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseDeleteChoiceRequest } as DeleteChoiceRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DeleteChoiceRequest {
    const message = { ...baseDeleteChoiceRequest } as DeleteChoiceRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: DeleteChoiceRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<DeleteChoiceRequest>): DeleteChoiceRequest {
    const message = { ...baseDeleteChoiceRequest } as DeleteChoiceRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseChoice: object = {
  id: 0,
  questionId: 0,
  isCorrect: false,
  format: 0,
  content: '',
};

export const Choice = {
  encode(
    message: Choice,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.questionId !== 0) {
      writer.uint32(16).int32(message.questionId);
    }
    if (message.isCorrect === true) {
      writer.uint32(24).bool(message.isCorrect);
    }
    if (message.format !== 0) {
      writer.uint32(32).int32(message.format);
    }
    if (message.content !== '') {
      writer.uint32(42).string(message.content);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Choice {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseChoice } as Choice;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.questionId = reader.int32();
          break;
        case 3:
          message.isCorrect = reader.bool();
          break;
        case 4:
          message.format = reader.int32() as any;
          break;
        case 5:
          message.content = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Choice {
    const message = { ...baseChoice } as Choice;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    if (object.questionId !== undefined && object.questionId !== null) {
      message.questionId = Number(object.questionId);
    } else {
      message.questionId = 0;
    }
    if (object.isCorrect !== undefined && object.isCorrect !== null) {
      message.isCorrect = Boolean(object.isCorrect);
    } else {
      message.isCorrect = false;
    }
    if (object.format !== undefined && object.format !== null) {
      message.format = textFormatFromJSON(object.format);
    } else {
      message.format = 0;
    }
    if (object.content !== undefined && object.content !== null) {
      message.content = String(object.content);
    } else {
      message.content = '';
    }
    return message;
  },

  toJSON(message: Choice): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.questionId !== undefined && (obj.questionId = message.questionId);
    message.isCorrect !== undefined && (obj.isCorrect = message.isCorrect);
    message.format !== undefined &&
      (obj.format = textFormatToJSON(message.format));
    message.content !== undefined && (obj.content = message.content);
    return obj;
  },

  fromPartial(object: DeepPartial<Choice>): Choice {
    const message = { ...baseChoice } as Choice;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    if (object.questionId !== undefined && object.questionId !== null) {
      message.questionId = object.questionId;
    } else {
      message.questionId = 0;
    }
    if (object.isCorrect !== undefined && object.isCorrect !== null) {
      message.isCorrect = object.isCorrect;
    } else {
      message.isCorrect = false;
    }
    if (object.format !== undefined && object.format !== null) {
      message.format = object.format;
    } else {
      message.format = 0;
    }
    if (object.content !== undefined && object.content !== null) {
      message.content = object.content;
    } else {
      message.content = '';
    }
    return message;
  },
};

export const ChoiceServiceService = {
  getChoice: {
    path: '/testycool.v1.ChoiceService/GetChoice',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetChoiceRequest) =>
      Buffer.from(GetChoiceRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetChoiceRequest.decode(value),
    responseSerialize: (value: GetChoiceResponse) =>
      Buffer.from(GetChoiceResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => GetChoiceResponse.decode(value),
  },
  listChoices: {
    path: '/testycool.v1.ChoiceService/ListChoices',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ListChoicesRequest) =>
      Buffer.from(ListChoicesRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => ListChoicesRequest.decode(value),
    responseSerialize: (value: ListChoicesResponse) =>
      Buffer.from(ListChoicesResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => ListChoicesResponse.decode(value),
  },
  createChoice: {
    path: '/testycool.v1.ChoiceService/CreateChoice',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CreateChoiceRequest) =>
      Buffer.from(CreateChoiceRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => CreateChoiceRequest.decode(value),
    responseSerialize: (value: CreateChoiceResponse) =>
      Buffer.from(CreateChoiceResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => CreateChoiceResponse.decode(value),
  },
  updateChoice: {
    path: '/testycool.v1.ChoiceService/UpdateChoice',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: UpdateChoiceRequest) =>
      Buffer.from(UpdateChoiceRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => UpdateChoiceRequest.decode(value),
    responseSerialize: (value: UpdateChoiceResponse) =>
      Buffer.from(UpdateChoiceResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => UpdateChoiceResponse.decode(value),
  },
  deleteChoice: {
    path: '/testycool.v1.ChoiceService/DeleteChoice',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: DeleteChoiceRequest) =>
      Buffer.from(DeleteChoiceRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => DeleteChoiceRequest.decode(value),
    responseSerialize: (value: Empty) =>
      Buffer.from(Empty.encode(value).finish()),
    responseDeserialize: (value: Buffer) => Empty.decode(value),
  },
} as const;

export interface ChoiceServiceServer extends UntypedServiceImplementation {
  getChoice: handleUnaryCall<GetChoiceRequest, GetChoiceResponse>;
  listChoices: handleUnaryCall<ListChoicesRequest, ListChoicesResponse>;
  createChoice: handleUnaryCall<CreateChoiceRequest, CreateChoiceResponse>;
  updateChoice: handleUnaryCall<UpdateChoiceRequest, UpdateChoiceResponse>;
  deleteChoice: handleUnaryCall<DeleteChoiceRequest, Empty>;
}

export interface ChoiceServiceClient extends Client {
  getChoice(
    request: GetChoiceRequest,
    callback: (error: ServiceError | null, response: GetChoiceResponse) => void
  ): ClientUnaryCall;
  getChoice(
    request: GetChoiceRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: GetChoiceResponse) => void
  ): ClientUnaryCall;
  getChoice(
    request: GetChoiceRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: GetChoiceResponse) => void
  ): ClientUnaryCall;
  listChoices(
    request: ListChoicesRequest,
    callback: (
      error: ServiceError | null,
      response: ListChoicesResponse
    ) => void
  ): ClientUnaryCall;
  listChoices(
    request: ListChoicesRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ListChoicesResponse
    ) => void
  ): ClientUnaryCall;
  listChoices(
    request: ListChoicesRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ListChoicesResponse
    ) => void
  ): ClientUnaryCall;
  createChoice(
    request: CreateChoiceRequest,
    callback: (
      error: ServiceError | null,
      response: CreateChoiceResponse
    ) => void
  ): ClientUnaryCall;
  createChoice(
    request: CreateChoiceRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CreateChoiceResponse
    ) => void
  ): ClientUnaryCall;
  createChoice(
    request: CreateChoiceRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CreateChoiceResponse
    ) => void
  ): ClientUnaryCall;
  updateChoice(
    request: UpdateChoiceRequest,
    callback: (
      error: ServiceError | null,
      response: UpdateChoiceResponse
    ) => void
  ): ClientUnaryCall;
  updateChoice(
    request: UpdateChoiceRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: UpdateChoiceResponse
    ) => void
  ): ClientUnaryCall;
  updateChoice(
    request: UpdateChoiceRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: UpdateChoiceResponse
    ) => void
  ): ClientUnaryCall;
  deleteChoice(
    request: DeleteChoiceRequest,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteChoice(
    request: DeleteChoiceRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteChoice(
    request: DeleteChoiceRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
}

export const ChoiceServiceClient = makeGenericClientConstructor(
  ChoiceServiceService,
  'testycool.v1.ChoiceService'
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): ChoiceServiceClient;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
