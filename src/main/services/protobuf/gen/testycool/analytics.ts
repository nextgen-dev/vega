/* eslint-disable */
import Long from 'long';
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from '@grpc/grpc-js';
import _m0 from 'protobufjs/minimal';
import { Timestamp } from '../google/protobuf/timestamp';
import { Empty } from '../google/protobuf/empty';

export const protobufPackage = 'testycool.v1';

export interface GetAnalyticsRequest {
  id: number;
}

export interface GetAnalyticsResponse {
  analytics: Analytics | undefined;
}

export interface ListAnalyticsRequest {
  size: number;
  page: number;
  filter: ListAnalyticsRequest_Filter | undefined;
}

export interface ListAnalyticsRequest_Filter {
  examId?: number | undefined;
}

export interface ListAnalyticsResponse {
  analytics: Analytics[];
  size: number;
  page: number;
  totalSize: number;
}

export interface CreateAnalyticsRequest {
  participantId: number;
  examId: number;
  message: string;
  createdAt: Date | undefined;
}

export interface CreateAnalyticsResponse {
  analytics: Analytics | undefined;
}

export interface UpdateAnalyticsRequest {
  analytics: Analytics | undefined;
}

export interface UpdateAnalyticsResponse {
  analytics: Analytics | undefined;
}

export interface DeleteAnalyticsRequest {
  id: number;
}

export interface Analytics {
  id: number;
  participantId: number;
  examId: number;
  message: string;
  createdAt: Date | undefined;
}

const baseGetAnalyticsRequest: object = { id: 0 };

export const GetAnalyticsRequest = {
  encode(
    message: GetAnalyticsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetAnalyticsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetAnalyticsRequest } as GetAnalyticsRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAnalyticsRequest {
    const message = { ...baseGetAnalyticsRequest } as GetAnalyticsRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: GetAnalyticsRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<GetAnalyticsRequest>): GetAnalyticsRequest {
    const message = { ...baseGetAnalyticsRequest } as GetAnalyticsRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseGetAnalyticsResponse: object = {};

export const GetAnalyticsResponse = {
  encode(
    message: GetAnalyticsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.analytics !== undefined) {
      Analytics.encode(message.analytics, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): GetAnalyticsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetAnalyticsResponse } as GetAnalyticsResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.analytics = Analytics.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAnalyticsResponse {
    const message = { ...baseGetAnalyticsResponse } as GetAnalyticsResponse;
    if (object.analytics !== undefined && object.analytics !== null) {
      message.analytics = Analytics.fromJSON(object.analytics);
    } else {
      message.analytics = undefined;
    }
    return message;
  },

  toJSON(message: GetAnalyticsResponse): unknown {
    const obj: any = {};
    message.analytics !== undefined &&
      (obj.analytics = message.analytics
        ? Analytics.toJSON(message.analytics)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<GetAnalyticsResponse>): GetAnalyticsResponse {
    const message = { ...baseGetAnalyticsResponse } as GetAnalyticsResponse;
    if (object.analytics !== undefined && object.analytics !== null) {
      message.analytics = Analytics.fromPartial(object.analytics);
    } else {
      message.analytics = undefined;
    }
    return message;
  },
};

const baseListAnalyticsRequest: object = { size: 0, page: 0 };

export const ListAnalyticsRequest = {
  encode(
    message: ListAnalyticsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.size !== 0) {
      writer.uint32(8).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(16).int32(message.page);
    }
    if (message.filter !== undefined) {
      ListAnalyticsRequest_Filter.encode(
        message.filter,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListAnalyticsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseListAnalyticsRequest } as ListAnalyticsRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.size = reader.int32();
          break;
        case 2:
          message.page = reader.int32();
          break;
        case 3:
          message.filter = ListAnalyticsRequest_Filter.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAnalyticsRequest {
    const message = { ...baseListAnalyticsRequest } as ListAnalyticsRequest;
    if (object.size !== undefined && object.size !== null) {
      message.size = Number(object.size);
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = Number(object.page);
    } else {
      message.page = 0;
    }
    if (object.filter !== undefined && object.filter !== null) {
      message.filter = ListAnalyticsRequest_Filter.fromJSON(object.filter);
    } else {
      message.filter = undefined;
    }
    return message;
  },

  toJSON(message: ListAnalyticsRequest): unknown {
    const obj: any = {};
    message.size !== undefined && (obj.size = message.size);
    message.page !== undefined && (obj.page = message.page);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? ListAnalyticsRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<ListAnalyticsRequest>): ListAnalyticsRequest {
    const message = { ...baseListAnalyticsRequest } as ListAnalyticsRequest;
    if (object.size !== undefined && object.size !== null) {
      message.size = object.size;
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = object.page;
    } else {
      message.page = 0;
    }
    if (object.filter !== undefined && object.filter !== null) {
      message.filter = ListAnalyticsRequest_Filter.fromPartial(object.filter);
    } else {
      message.filter = undefined;
    }
    return message;
  },
};

const baseListAnalyticsRequest_Filter: object = {};

export const ListAnalyticsRequest_Filter = {
  encode(
    message: ListAnalyticsRequest_Filter,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examId !== undefined) {
      writer.uint32(16).int32(message.examId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListAnalyticsRequest_Filter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseListAnalyticsRequest_Filter,
    } as ListAnalyticsRequest_Filter;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 2:
          message.examId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAnalyticsRequest_Filter {
    const message = {
      ...baseListAnalyticsRequest_Filter,
    } as ListAnalyticsRequest_Filter;
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = Number(object.examId);
    } else {
      message.examId = undefined;
    }
    return message;
  },

  toJSON(message: ListAnalyticsRequest_Filter): unknown {
    const obj: any = {};
    message.examId !== undefined && (obj.examId = message.examId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ListAnalyticsRequest_Filter>
  ): ListAnalyticsRequest_Filter {
    const message = {
      ...baseListAnalyticsRequest_Filter,
    } as ListAnalyticsRequest_Filter;
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = object.examId;
    } else {
      message.examId = undefined;
    }
    return message;
  },
};

const baseListAnalyticsResponse: object = { size: 0, page: 0, totalSize: 0 };

export const ListAnalyticsResponse = {
  encode(
    message: ListAnalyticsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.analytics) {
      Analytics.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.size !== 0) {
      writer.uint32(16).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(24).int32(message.page);
    }
    if (message.totalSize !== 0) {
      writer.uint32(32).int32(message.totalSize);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListAnalyticsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseListAnalyticsResponse } as ListAnalyticsResponse;
    message.analytics = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.analytics.push(Analytics.decode(reader, reader.uint32()));
          break;
        case 2:
          message.size = reader.int32();
          break;
        case 3:
          message.page = reader.int32();
          break;
        case 4:
          message.totalSize = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAnalyticsResponse {
    const message = { ...baseListAnalyticsResponse } as ListAnalyticsResponse;
    message.analytics = [];
    if (object.analytics !== undefined && object.analytics !== null) {
      for (const e of object.analytics) {
        message.analytics.push(Analytics.fromJSON(e));
      }
    }
    if (object.size !== undefined && object.size !== null) {
      message.size = Number(object.size);
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = Number(object.page);
    } else {
      message.page = 0;
    }
    if (object.totalSize !== undefined && object.totalSize !== null) {
      message.totalSize = Number(object.totalSize);
    } else {
      message.totalSize = 0;
    }
    return message;
  },

  toJSON(message: ListAnalyticsResponse): unknown {
    const obj: any = {};
    if (message.analytics) {
      obj.analytics = message.analytics.map((e) =>
        e ? Analytics.toJSON(e) : undefined
      );
    } else {
      obj.analytics = [];
    }
    message.size !== undefined && (obj.size = message.size);
    message.page !== undefined && (obj.page = message.page);
    message.totalSize !== undefined && (obj.totalSize = message.totalSize);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ListAnalyticsResponse>
  ): ListAnalyticsResponse {
    const message = { ...baseListAnalyticsResponse } as ListAnalyticsResponse;
    message.analytics = [];
    if (object.analytics !== undefined && object.analytics !== null) {
      for (const e of object.analytics) {
        message.analytics.push(Analytics.fromPartial(e));
      }
    }
    if (object.size !== undefined && object.size !== null) {
      message.size = object.size;
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = object.page;
    } else {
      message.page = 0;
    }
    if (object.totalSize !== undefined && object.totalSize !== null) {
      message.totalSize = object.totalSize;
    } else {
      message.totalSize = 0;
    }
    return message;
  },
};

const baseCreateAnalyticsRequest: object = {
  participantId: 0,
  examId: 0,
  message: '',
};

export const CreateAnalyticsRequest = {
  encode(
    message: CreateAnalyticsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participantId !== 0) {
      writer.uint32(8).int32(message.participantId);
    }
    if (message.examId !== 0) {
      writer.uint32(16).int32(message.examId);
    }
    if (message.message !== '') {
      writer.uint32(26).string(message.message);
    }
    if (message.createdAt !== undefined) {
      Timestamp.encode(
        toTimestamp(message.createdAt),
        writer.uint32(34).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateAnalyticsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCreateAnalyticsRequest } as CreateAnalyticsRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participantId = reader.int32();
          break;
        case 2:
          message.examId = reader.int32();
          break;
        case 3:
          message.message = reader.string();
          break;
        case 4:
          message.createdAt = fromTimestamp(
            Timestamp.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateAnalyticsRequest {
    const message = { ...baseCreateAnalyticsRequest } as CreateAnalyticsRequest;
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = Number(object.participantId);
    } else {
      message.participantId = 0;
    }
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = Number(object.examId);
    } else {
      message.examId = 0;
    }
    if (object.message !== undefined && object.message !== null) {
      message.message = String(object.message);
    } else {
      message.message = '';
    }
    if (object.createdAt !== undefined && object.createdAt !== null) {
      message.createdAt = fromJsonTimestamp(object.createdAt);
    } else {
      message.createdAt = undefined;
    }
    return message;
  },

  toJSON(message: CreateAnalyticsRequest): unknown {
    const obj: any = {};
    message.participantId !== undefined &&
      (obj.participantId = message.participantId);
    message.examId !== undefined && (obj.examId = message.examId);
    message.message !== undefined && (obj.message = message.message);
    message.createdAt !== undefined &&
      (obj.createdAt = message.createdAt.toISOString());
    return obj;
  },

  fromPartial(
    object: DeepPartial<CreateAnalyticsRequest>
  ): CreateAnalyticsRequest {
    const message = { ...baseCreateAnalyticsRequest } as CreateAnalyticsRequest;
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = object.participantId;
    } else {
      message.participantId = 0;
    }
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = object.examId;
    } else {
      message.examId = 0;
    }
    if (object.message !== undefined && object.message !== null) {
      message.message = object.message;
    } else {
      message.message = '';
    }
    if (object.createdAt !== undefined && object.createdAt !== null) {
      message.createdAt = object.createdAt;
    } else {
      message.createdAt = undefined;
    }
    return message;
  },
};

const baseCreateAnalyticsResponse: object = {};

export const CreateAnalyticsResponse = {
  encode(
    message: CreateAnalyticsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.analytics !== undefined) {
      Analytics.encode(message.analytics, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateAnalyticsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseCreateAnalyticsResponse,
    } as CreateAnalyticsResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.analytics = Analytics.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateAnalyticsResponse {
    const message = {
      ...baseCreateAnalyticsResponse,
    } as CreateAnalyticsResponse;
    if (object.analytics !== undefined && object.analytics !== null) {
      message.analytics = Analytics.fromJSON(object.analytics);
    } else {
      message.analytics = undefined;
    }
    return message;
  },

  toJSON(message: CreateAnalyticsResponse): unknown {
    const obj: any = {};
    message.analytics !== undefined &&
      (obj.analytics = message.analytics
        ? Analytics.toJSON(message.analytics)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CreateAnalyticsResponse>
  ): CreateAnalyticsResponse {
    const message = {
      ...baseCreateAnalyticsResponse,
    } as CreateAnalyticsResponse;
    if (object.analytics !== undefined && object.analytics !== null) {
      message.analytics = Analytics.fromPartial(object.analytics);
    } else {
      message.analytics = undefined;
    }
    return message;
  },
};

const baseUpdateAnalyticsRequest: object = {};

export const UpdateAnalyticsRequest = {
  encode(
    message: UpdateAnalyticsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.analytics !== undefined) {
      Analytics.encode(message.analytics, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateAnalyticsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUpdateAnalyticsRequest } as UpdateAnalyticsRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.analytics = Analytics.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateAnalyticsRequest {
    const message = { ...baseUpdateAnalyticsRequest } as UpdateAnalyticsRequest;
    if (object.analytics !== undefined && object.analytics !== null) {
      message.analytics = Analytics.fromJSON(object.analytics);
    } else {
      message.analytics = undefined;
    }
    return message;
  },

  toJSON(message: UpdateAnalyticsRequest): unknown {
    const obj: any = {};
    message.analytics !== undefined &&
      (obj.analytics = message.analytics
        ? Analytics.toJSON(message.analytics)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<UpdateAnalyticsRequest>
  ): UpdateAnalyticsRequest {
    const message = { ...baseUpdateAnalyticsRequest } as UpdateAnalyticsRequest;
    if (object.analytics !== undefined && object.analytics !== null) {
      message.analytics = Analytics.fromPartial(object.analytics);
    } else {
      message.analytics = undefined;
    }
    return message;
  },
};

const baseUpdateAnalyticsResponse: object = {};

export const UpdateAnalyticsResponse = {
  encode(
    message: UpdateAnalyticsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.analytics !== undefined) {
      Analytics.encode(message.analytics, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateAnalyticsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseUpdateAnalyticsResponse,
    } as UpdateAnalyticsResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.analytics = Analytics.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateAnalyticsResponse {
    const message = {
      ...baseUpdateAnalyticsResponse,
    } as UpdateAnalyticsResponse;
    if (object.analytics !== undefined && object.analytics !== null) {
      message.analytics = Analytics.fromJSON(object.analytics);
    } else {
      message.analytics = undefined;
    }
    return message;
  },

  toJSON(message: UpdateAnalyticsResponse): unknown {
    const obj: any = {};
    message.analytics !== undefined &&
      (obj.analytics = message.analytics
        ? Analytics.toJSON(message.analytics)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<UpdateAnalyticsResponse>
  ): UpdateAnalyticsResponse {
    const message = {
      ...baseUpdateAnalyticsResponse,
    } as UpdateAnalyticsResponse;
    if (object.analytics !== undefined && object.analytics !== null) {
      message.analytics = Analytics.fromPartial(object.analytics);
    } else {
      message.analytics = undefined;
    }
    return message;
  },
};

const baseDeleteAnalyticsRequest: object = { id: 0 };

export const DeleteAnalyticsRequest = {
  encode(
    message: DeleteAnalyticsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): DeleteAnalyticsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseDeleteAnalyticsRequest } as DeleteAnalyticsRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DeleteAnalyticsRequest {
    const message = { ...baseDeleteAnalyticsRequest } as DeleteAnalyticsRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: DeleteAnalyticsRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(
    object: DeepPartial<DeleteAnalyticsRequest>
  ): DeleteAnalyticsRequest {
    const message = { ...baseDeleteAnalyticsRequest } as DeleteAnalyticsRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseAnalytics: object = {
  id: 0,
  participantId: 0,
  examId: 0,
  message: '',
};

export const Analytics = {
  encode(
    message: Analytics,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.participantId !== 0) {
      writer.uint32(16).int32(message.participantId);
    }
    if (message.examId !== 0) {
      writer.uint32(24).int32(message.examId);
    }
    if (message.message !== '') {
      writer.uint32(34).string(message.message);
    }
    if (message.createdAt !== undefined) {
      Timestamp.encode(
        toTimestamp(message.createdAt),
        writer.uint32(42).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Analytics {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAnalytics } as Analytics;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.participantId = reader.int32();
          break;
        case 3:
          message.examId = reader.int32();
          break;
        case 4:
          message.message = reader.string();
          break;
        case 5:
          message.createdAt = fromTimestamp(
            Timestamp.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Analytics {
    const message = { ...baseAnalytics } as Analytics;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = Number(object.participantId);
    } else {
      message.participantId = 0;
    }
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = Number(object.examId);
    } else {
      message.examId = 0;
    }
    if (object.message !== undefined && object.message !== null) {
      message.message = String(object.message);
    } else {
      message.message = '';
    }
    if (object.createdAt !== undefined && object.createdAt !== null) {
      message.createdAt = fromJsonTimestamp(object.createdAt);
    } else {
      message.createdAt = undefined;
    }
    return message;
  },

  toJSON(message: Analytics): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.participantId !== undefined &&
      (obj.participantId = message.participantId);
    message.examId !== undefined && (obj.examId = message.examId);
    message.message !== undefined && (obj.message = message.message);
    message.createdAt !== undefined &&
      (obj.createdAt = message.createdAt.toISOString());
    return obj;
  },

  fromPartial(object: DeepPartial<Analytics>): Analytics {
    const message = { ...baseAnalytics } as Analytics;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = object.participantId;
    } else {
      message.participantId = 0;
    }
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = object.examId;
    } else {
      message.examId = 0;
    }
    if (object.message !== undefined && object.message !== null) {
      message.message = object.message;
    } else {
      message.message = '';
    }
    if (object.createdAt !== undefined && object.createdAt !== null) {
      message.createdAt = object.createdAt;
    } else {
      message.createdAt = undefined;
    }
    return message;
  },
};

export const AnalyticsServiceService = {
  getAnalytics: {
    path: '/testycool.v1.AnalyticsService/GetAnalytics',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetAnalyticsRequest) =>
      Buffer.from(GetAnalyticsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetAnalyticsRequest.decode(value),
    responseSerialize: (value: GetAnalyticsResponse) =>
      Buffer.from(GetAnalyticsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => GetAnalyticsResponse.decode(value),
  },
  listAnalytics: {
    path: '/testycool.v1.AnalyticsService/ListAnalytics',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ListAnalyticsRequest) =>
      Buffer.from(ListAnalyticsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => ListAnalyticsRequest.decode(value),
    responseSerialize: (value: ListAnalyticsResponse) =>
      Buffer.from(ListAnalyticsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => ListAnalyticsResponse.decode(value),
  },
  createAnalytics: {
    path: '/testycool.v1.AnalyticsService/CreateAnalytics',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CreateAnalyticsRequest) =>
      Buffer.from(CreateAnalyticsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => CreateAnalyticsRequest.decode(value),
    responseSerialize: (value: CreateAnalyticsResponse) =>
      Buffer.from(CreateAnalyticsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      CreateAnalyticsResponse.decode(value),
  },
  updateAnalytics: {
    path: '/testycool.v1.AnalyticsService/UpdateAnalytics',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: UpdateAnalyticsRequest) =>
      Buffer.from(UpdateAnalyticsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => UpdateAnalyticsRequest.decode(value),
    responseSerialize: (value: UpdateAnalyticsResponse) =>
      Buffer.from(UpdateAnalyticsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      UpdateAnalyticsResponse.decode(value),
  },
  deleteAnalytics: {
    path: '/testycool.v1.AnalyticsService/DeleteAnalytics',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: DeleteAnalyticsRequest) =>
      Buffer.from(DeleteAnalyticsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => DeleteAnalyticsRequest.decode(value),
    responseSerialize: (value: Empty) =>
      Buffer.from(Empty.encode(value).finish()),
    responseDeserialize: (value: Buffer) => Empty.decode(value),
  },
} as const;

export interface AnalyticsServiceServer extends UntypedServiceImplementation {
  getAnalytics: handleUnaryCall<GetAnalyticsRequest, GetAnalyticsResponse>;
  listAnalytics: handleUnaryCall<ListAnalyticsRequest, ListAnalyticsResponse>;
  createAnalytics: handleUnaryCall<
    CreateAnalyticsRequest,
    CreateAnalyticsResponse
  >;
  updateAnalytics: handleUnaryCall<
    UpdateAnalyticsRequest,
    UpdateAnalyticsResponse
  >;
  deleteAnalytics: handleUnaryCall<DeleteAnalyticsRequest, Empty>;
}

export interface AnalyticsServiceClient extends Client {
  getAnalytics(
    request: GetAnalyticsRequest,
    callback: (
      error: ServiceError | null,
      response: GetAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  getAnalytics(
    request: GetAnalyticsRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: GetAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  getAnalytics(
    request: GetAnalyticsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: GetAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  listAnalytics(
    request: ListAnalyticsRequest,
    callback: (
      error: ServiceError | null,
      response: ListAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  listAnalytics(
    request: ListAnalyticsRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ListAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  listAnalytics(
    request: ListAnalyticsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ListAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  createAnalytics(
    request: CreateAnalyticsRequest,
    callback: (
      error: ServiceError | null,
      response: CreateAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  createAnalytics(
    request: CreateAnalyticsRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CreateAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  createAnalytics(
    request: CreateAnalyticsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CreateAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  updateAnalytics(
    request: UpdateAnalyticsRequest,
    callback: (
      error: ServiceError | null,
      response: UpdateAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  updateAnalytics(
    request: UpdateAnalyticsRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: UpdateAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  updateAnalytics(
    request: UpdateAnalyticsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: UpdateAnalyticsResponse
    ) => void
  ): ClientUnaryCall;
  deleteAnalytics(
    request: DeleteAnalyticsRequest,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteAnalytics(
    request: DeleteAnalyticsRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteAnalytics(
    request: DeleteAnalyticsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
}

export const AnalyticsServiceClient = makeGenericClientConstructor(
  AnalyticsServiceService,
  'testycool.v1.AnalyticsService'
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): AnalyticsServiceClient;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function toTimestamp(date: Date): Timestamp {
  const seconds = date.getTime() / 1_000;
  const nanos = (date.getTime() % 1_000) * 1_000_000;
  return { seconds, nanos };
}

function fromTimestamp(t: Timestamp): Date {
  let millis = t.seconds * 1_000;
  millis += t.nanos / 1_000_000;
  return new Date(millis);
}

function fromJsonTimestamp(o: any): Date {
  if (o instanceof Date) {
    return o;
  } else if (typeof o === 'string') {
    return new Date(o);
  } else {
    return fromTimestamp(Timestamp.fromJSON(o));
  }
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
