import configureElectronStore from '../shared/store/configureElectronStore';

const mainStore = configureElectronStore('main');

export default mainStore;
