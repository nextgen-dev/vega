import { ipcMain } from 'electron';
import { QuestionModel } from '../../shared/models/QuestionModel';
import mainStore from '../store';
import Services from '../services/services';
import { ChannelCredentials, Metadata } from '@grpc/grpc-js';
import {
  CreateQuestionRequest,
  DeleteQuestionRequest,
  QuestionServiceClient,
} from '../services/protobuf/gen/testycool/question';
import { TypeModelToNumber } from '../../shared/libs/QuestionTypeParser';
import { FormatModelToNumber } from '../../shared/libs/TextFormatTypeParser';

export default function question() {
  ipcMain.on(
    'list-questions',
    (event, args: { size: number; page: number; examId: number }) => {
      const { address } = mainStore.getState().AuthReducer;
      if (!Services.question && address) {
        Services.question = new QuestionServiceClient(
          address,
          ChannelCredentials.createInsecure()
        );
      }
      const token = mainStore.getState().AuthReducer.accessToken;
      const metadata = new Metadata();
      metadata.set('authorization', `Bearer ${token}`);
      Services.question?.listQuestions(
        {
          size: args.size,
          page: args.page,
          filter: { examId: args.examId },
        },
        metadata,
        (err, response) => {
          if (err) {
            event.sender.send('question-error', {
              code: err.code as number,
              details: err.details as string,
            });
          }
          if (response) {
            event.reply('list-questions', { ...response, examId: args.examId });
          }
        }
      );
    }
  );
  ipcMain.on('get-question', (event, args: { id: number }) => {
    const token = mainStore.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('authorization', `Bearer ${token}`);
    Services.question?.getQuestion(
      {
        id: args.id,
      },
      metadata,
      (err, response) => {
        if (err) {
          event.sender.send('question-error', {
            code: err.code as number,
            details: err.details as string,
          });
        }
        if (response) {
          event.reply('get-question', response);
        }
      }
    );
  });
  ipcMain.on(
    'create-question',
    (event, args: { newQuestion: CreateQuestionRequest }) => {
      const token = mainStore.getState().AuthReducer.accessToken;
      const metadata = new Metadata();
      metadata.set('authorization', `Bearer ${token}`);
      Services.question?.createQuestion(
        {
          ...args.newQuestion,
        },
        metadata,
        (err, response) => {
          if (err) {
            event.sender.send('question-error', {
              code: err.code as number,
              details: err.details as string,
            });
          }
          if (response) {
            event.reply('create-question', response);
          }
        }
      );
    }
  );
  ipcMain.on(
    'update-question',
    (
      event,
      args: {
        updatedQuestion: QuestionModel;
      }
    ) => {
      const token = mainStore.getState().AuthReducer.accessToken;
      const metadata = new Metadata();
      metadata.set('authorization', `Bearer ${token}`);
      Services.question?.updateQuestion(
        {
          question: {
            ...args.updatedQuestion,
            type: TypeModelToNumber(args.updatedQuestion.type),
            format: FormatModelToNumber(args.updatedQuestion.format),
          },
        },
        metadata,
        (err, response) => {
          if (err) {
            event.sender.send('question-error', {
              code: err.code as number,
              details: err.details as string,
            });
          }
          if (response) {
            event.reply('update-question', response);
          }
        }
      );
    }
  );
  ipcMain.on(
    'update-question-type',
    (
      event,
      args: {
        updatedQuestion: QuestionModel;
      }
    ) => {
      const token = mainStore.getState().AuthReducer.accessToken;
      const metadata = new Metadata();
      metadata.set('authorization', `Bearer ${token}`);
      Services.question?.deleteQuestion(
        { id: args.updatedQuestion.id },
        metadata,
        (err, response) => {
          if (err) {
            event.sender.send('question-error', {
              code: err.code as number,
              details: err.details as string,
            });
          }
          if (response) {
            Services.question?.createQuestion(
              {
                examId: args.updatedQuestion.examId,
                type: TypeModelToNumber(args.updatedQuestion.type),
                content: args.updatedQuestion.content,
                format: FormatModelToNumber(args.updatedQuestion.format),
              },
              metadata,
              (createErr, createResponse) => {
                if (createErr) {
                  event.sender.send('question-error', {
                    code: createErr.code as number,
                    details: createErr.details as string,
                  });
                }
                if (createResponse) {
                  event.reply('update-question-type', {
                    ...createResponse,
                    oldId: args.updatedQuestion.id,
                  });
                }
              }
            );
          }
        }
      );
    }
  );
  ipcMain.on('delete-question', (event, args: DeleteQuestionRequest) => {
    const token = mainStore.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('authorization', `Bearer ${token}`);
    Services.question?.deleteQuestion(args, metadata, (err, response) => {
      if (err) {
        event.sender.send('question-error', {
          code: err.code as number,
          details: err.details as string,
        });
      }
      if (response) {
        event.reply('delete-question', { id: args.id });
      }
    });
  });
}
