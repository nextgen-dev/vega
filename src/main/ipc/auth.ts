import { ipcMain } from 'electron';
import { ChannelCredentials } from '@grpc/grpc-js';
import Services from '../services/services';
import { setAccessToken, setAddress } from '../../shared/actions/AuthAction';
import mainStore from '../store';
import { AuthServiceClient } from '../services/protobuf/gen/testycool/auth';

const auth = () => {
  ipcMain.on(
    'auth-request',
    (event, args: { address: string; passcode: string }) => {
      if (
        !Services.auth ||
        args.address !== mainStore.getState().AuthReducer.address
      ) {
        mainStore.dispatch(setAddress({ address: args.address }));
        Services.auth = new AuthServiceClient(
          args.address,
          ChannelCredentials.createInsecure()
        );
      }

      Services.auth?.getAdminToken(
        { passcode: args.passcode },
        (err, response) => {
          if (err) {
            event.sender.send('auth-error', {
              code: err.code as number,
              details: err.details as string,
            });
          }
          if (response) {
            mainStore.dispatch(
              setAccessToken({ accessToken: response.accessToken })
            );
            event.reply('auth-request', response);
          }
        }
      );
    }
  );
};

export default auth;
