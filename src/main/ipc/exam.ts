import { ipcMain } from 'electron';
import { ChannelCredentials, Metadata } from '@grpc/grpc-js';
import Services from '../services/services';
import mainStore from '../store';
import {
  Exam,
  Exam_Status,
  DeleteExamRequest,
  ExamServiceClient,
  ListExamsRequest,
  GetExamRequest,
  CreateExamRequest,
} from '../services/protobuf/gen/testycool/exam';
import { ExamStatusModel } from '../../shared/models/ExamModel';

const exam = () => {
  ipcMain.on('list-exams', (event, args: ListExamsRequest) => {
    const { address } = mainStore.getState().AuthReducer;
    if (!Services.exam && address) {
      Services.exam = new ExamServiceClient(
        address,
        ChannelCredentials.createInsecure()
      );
    }
    const token = mainStore.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('authorization', `Bearer ${token}`);
    Services.exam?.listExams(args, metadata, (err, response) => {
      if (err) {
        event.sender.send('exam-error', {
          code: err.code as number,
          details: err.details as string,
        });
      }
      if (response) {
        event.reply('list-exams', response);
      }
    });
  });
  ipcMain.on('get-exam', (event, args: GetExamRequest) => {
    const token = mainStore.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('authorization', `Bearer ${token}`);
    Services.exam?.getExam(
      { id: args.id, password: undefined },
      metadata,
      (err, response) => {
        if (err) {
          event.sender.send('exam-error', {
            code: err.code as number,
            details: err.details as string,
          });
        }
        if (response) {
          event.reply('get-exam', response);
        }
      }
    );
  });
  ipcMain.on('create-exam', (event, args: { newExam: CreateExamRequest }) => {
    const token = mainStore.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('authorization', `Bearer ${token}`);
    Services.exam?.createExam(args.newExam, metadata, (err, response) => {
      if (err) {
        event.sender.send('exam-error', {
          code: err.code as number,
          details: err.details as string,
        });
      }
      if (response) {
        event.reply('create-exam', response);
      }
    });
  });
  ipcMain.on('update-exam', (event, args: { updatedExam: Exam }) => {
    const convertStatus = (status: number) => {
      switch (status) {
        case ExamStatusModel.WAITING:
          return Exam_Status.WAITING;
        case ExamStatusModel.STARTED:
          return Exam_Status.STARTED;
        case ExamStatusModel.DONE:
          return Exam_Status.DONE;
        default:
          return Exam_Status.UNKNOWN;
      }
    };

    const token = mainStore.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('authorization', `Bearer ${token}`);
    Services.exam?.updateExam(
      {
        exam: {
          ...args.updatedExam,
          status: convertStatus(args.updatedExam.status),
        },
      },
      metadata,
      (err, response) => {
        if (err) {
          event.sender.send('exam-error', {
            code: err.code as number,
            details: err.details as string,
          });
        }
        if (response) {
          event.reply('update-exam', response);
        }
      }
    );
  });
  ipcMain.on('delete-exam', (event, args: DeleteExamRequest) => {
    const token = mainStore.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('authorization', `Bearer ${token}`);
    Services.exam?.deleteExam(args, metadata, (err, response) => {
      if (err) {
        event.sender.send('exam-error', {
          code: err.code as number,
          details: err.details as string,
        });
      }
      if (response) {
        event.reply('delete-exam', { id: args.id });
      }
    });
  });
};

export default exam;
