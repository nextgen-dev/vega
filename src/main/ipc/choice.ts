import { ipcMain } from 'electron';
import {
  ChoiceServiceClient,
  CreateChoiceRequest,
  DeleteChoiceRequest,
  ListChoicesRequest,
} from '../services/protobuf/gen/testycool/choice';
import ChoiceModel from '../../shared/models/ChoiceModel';
import { FormatModelToNumber } from '../../shared/libs/TextFormatTypeParser';
import mainStore from '../store';
import Services from '../services/services';
import { ChannelCredentials, Metadata } from '@grpc/grpc-js';

export default function choice() {
  ipcMain.on('list-choices', (event, args: ListChoicesRequest) => {
    const { address } = mainStore.getState().AuthReducer;
    if (!Services.choice && address) {
      Services.choice = new ChoiceServiceClient(
        address,
        ChannelCredentials.createInsecure()
      );
    }
    const token = mainStore.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('authorization', `Bearer ${token}`);
    Services.choice?.listChoices(
      {
        size: args.size,
        page: args.page,
        filter: { questionId: args.filter?.questionId },
      },
      metadata,
      (err, response) => {
        if (err) {
          event.sender.send('choice-error', {
            code: err.code as number,
            details: err.details as string,
          });
        }
        if (response) {
          event.reply('list-choices', {
            ...response,
            questionId: args.filter?.questionId,
          });
        }
      }
    );
  });
  ipcMain.on(
    'create-choice',
    (event, args: { newChoice: CreateChoiceRequest }) => {
      const token = mainStore.getState().AuthReducer.accessToken;
      const metadata = new Metadata();
      metadata.set('authorization', `Bearer ${token}`);
      Services.choice?.createChoice(
        {
          ...args.newChoice,
        },
        metadata,
        (err, response) => {
          if (err) {
            event.sender.send('choice-error', {
              code: err.code as number,
              details: err.details as string,
            });
          }
          if (response) {
            event.reply('create-choice', response);
          }
        }
      );
    }
  );
  ipcMain.on(
    'update-choice',
    (
      event,
      args: {
        updatedChoice: ChoiceModel;
      }
    ) => {
      const token = mainStore.getState().AuthReducer.accessToken;
      const metadata = new Metadata();
      metadata.set('authorization', `Bearer ${token}`);
      Services.choice?.updateChoice(
        {
          choice: {
            ...args.updatedChoice,
            format: FormatModelToNumber(args.updatedChoice.format),
          },
        },
        metadata,
        (err, response) => {
          if (err) {
            event.sender.send('choice-error', {
              code: err.code as number,
              details: err.details as string,
            });
          }
          if (response) {
            event.reply('update-choice', response);
          }
        }
      );
    }
  );
  ipcMain.on('delete-choice', (event, args: DeleteChoiceRequest) => {
    console.log(`delete ${args.id}`);
    event.reply('delete-choice', args.id);
  });
}
